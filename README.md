# Riemannian preconditioned algorithms for tensor completion (TC-Precon)

This package contains a matlab implementation of algorithms for tensor completion presented in the report "New Riemannian preconditioned algorithms for tensor completion via polyadic decomposition" by Shuyu Dong, Bin Gao, Yu Guan, and François Glineur; [arXiv preprint arXiv:2101.11108](https://arxiv.org/pdf/2101.11108.pdf), pages 1--24, 2021. 
    

## Installation:

1) Run `startup.m` 
2) Run `compile_mex.m`: this step is optional; the mexfunctions are pre-compiled under `tcprecon-core/mex/bin` 

## Examples 

1) Run `test_lowtcrank_n0.m`. There are plots at the end. 
2) Run `test_lowtcrank_noisy.m`. There are plots at the end. 
3) Run `run*ml1m.m`. 


## External tools

All files are written by the authors except a few functions for 
  - tensor data structures (for input data) from Tensor-toolbox, 
  - manifold objects from [Manopt](http://manopt.org). 

These functions are included in `tools/`. 


