function [X, cpd_tensor] = get_diagttensor_from_cpd(cpd_tensor)
%   GeomCG Tensor Completion. Copyright 2013 by
%   Michael Steinlechner
%   Questions and contact: michael.steinlechner@epfl.ch
%   BSD 2-clause license, see LICENSE.txt
%
%    Modified by SD for converting a tensor in CPD format into a Tucker format
%    with a diagonal core.

% Create the diagonal sparse tensor
if nargin < 1 
    cpd_tensor = {};
    m = [50 200 100]; 
    R = 10; 
    k = numel(m); 
    for i = 1:k
       cpd_tensor{i} = randn(m(i), R); % randomly generate each factor
    end
end
    
    k = numel(cpd_tensor); 
    if k ~= 3 
        error('this function only deals with 3rd order tensors\n');
    end
    dim_cube = size(cpd_tensor{1}, 2); 
    subs = repmat([1:dim_cube]', [1, k]); 
    vals = ones(dim_cube,1); 
    sz = dim_cube*ones(1, k); 
    spcore = sptensor(subs, vals, sz);  %X = SPTENSOR(SUBS, VALS, SZ, FUN)
    core = tensor(spcore);
       
    X.U1 = cpd_tensor{1};
    X.U2 = cpd_tensor{2};
    X.U3 = cpd_tensor{3};
    X.G = core.data; 
    
end

