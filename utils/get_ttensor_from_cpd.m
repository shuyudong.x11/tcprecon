function [X, cpd_tensor] = get_ttensor_from_cpd(cpd_tensor, dims_core)
% Generate a tensor in Tucker form from a given tensor in CPD form. This conversion is 
% only an approximation based on the fact that the CPD form can be rewritten in the special 
% Tucker form [[D; U1, U2, U3], where D is a diagonal cubic core tensor.
%  
% This function modified from MAKERANDTENSOR.m
% ------------------------------------------------------
%   X = MAKERANDTENSOR( N, K ) creates a random Tucker tensor X stored as a 
%   ttensor object. The entries of the core tensor the basis factors are chosen
%   independently from the uniform distribution on [0,1]. Finally, the basis factors
%   are orthogonalized using a QR procedure.
%
%   See also makeOmegaSet
%

%   GeomCG Tensor Completion. Copyright 2013 by
%   Michael Steinlechner
%   Questions and contact: michael.steinlechner@epfl.ch
%   BSD 2-clause license, see LICENSE.txt
%
%    Modified by BM to be independent of tensor toolbox, 2015.
% ------------------------------------------------------
% Latest version: June 2020. 
% shuyu.dong@uclouvain.be 
% 

    ndims = numel(cpd_tensor); 
    if ndims ~= 3 
        error('this function only deals with 3rd order tensors\n');
    end

    if nargin < 2
        % this is the case of "eqrks", where ri = rank_cpd, for i = 1,..,k.
        k = repmat(size(cpd_tensor{1}, 2), [1, ndims]);
    else
        k = dims_core;
    end
    r_cpd = size(cpd_tensor{1}, 2); 


    [U1,R1] = qr( cpd_tensor{1}, 0);
    [U2,R2] = qr( cpd_tensor{2}, 0);
    [U3,R3] = qr( cpd_tensor{3}, 0);

    C  = sqrt(r_cpd/prod(k))*rand(k); % Modified: replace tenrand( k );
    
    % Lines by MS
    % C = ttm( C, {R1,R2,R3},[1,2,3]);
    % X = ttensor( C, {U1, U2, U3} );
    
    % Modified
    r1 = k(1);
    r2 = k(2);
    r3 = k(3);
    
    % Multplication by R1
    C1 = reshape(C, r1, r2*r3);
    CR1 = reshape(R1*C1, r1, r2, r3);
    
    % Multplication by R2
    C2 = reshape(permute(CR1, [2 1 3]), r2, r1*r3); 
    CR1R2 = permute(reshape(R2*C2, r2, r1, r3), [2 1 3]);
    
    % Multplication by R3
    C3 = reshape(permute(CR1R2, [3 1 2]), r3, r1*r2);  
        
    CR1R2R3 = permute(reshape(R3*C3, r3, r1, r2), [2 3 1]); 
        
    X.U1 = U1;
    X.U2 = U2;
    X.U3 = U3;
    X.G = CR1R2R3;
    
end
