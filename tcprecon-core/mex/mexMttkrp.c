/*=================================================================
% function A = mexMttkrp(S, I, J, K, U1, U2, U3)
% This function computes the Matricization Times Khatri-Rao Product, where the tensor is a sparse 3-dimensional tensor.
%    S_{(1)}*KRprod(U3, U2), 
% where the tensor S is sparse with non-zero entries indexed by the three nnz-by-1 arrays I, J and K. 
% I, J and K must be UINT32 arrays of size nnz-by-1.
%
% U1: n1-by-r, real, double
% U2: n2-by-r, real, double 
% U3: n3-by-r, real, double 
% I: nnz-by-1 row indices, uint32
% J: nnz-by-1 column indices, uint32
% K: nnz-by-1 3rd dimension indices, uint32
%
% Complexity: O(nnz(S)*r)
%
% Warning: no check of data consistency is performed. Matlab will
% most likely crash if I, J or K go out of bounds.
%
% Compile with: mex mexMttkrp.c -largeArrayDims
%  
% May, 2020. Shuyu Dong, UCLouvain. 
% Contact: shuyu.dong@uclouvain.be 
 *=================================================================*/

/* #include <math.h> */
#include "mex.h"
#include "matrix.h"

/* Input Arguments */

#define	pS	prhs[0]
#define	pI	prhs[1]
#define	pJ	prhs[2]
#define	pK	prhs[3]

#define	pU1	prhs[4]
#define	pU2	prhs[5]
#define	pU3	prhs[6]

/* Output Arguments */
#define	pA	plhs[0]

void mexFunction(
          int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray* prhs[] )
{
    uint32_T *I, *J, *K;
    double *S, *U1, *U2, *U3, *A;
    mwSize n1, n2, n3, r, nnz;
    // mwIndex k, it;
    
    /* Check for proper number of arguments */
    if (nrhs != 7) { 
        mexErrMsgTxt("Seven input arguments are required."); 
    } else if (nlhs != 1) {
        mexErrMsgTxt("A single output argument is required."); 
    } 
    
    /* Check argument classes */
    if(!mxIsDouble(pU1) || !mxIsDouble(pU2) || !mxIsDouble(pU3)) {
        mexErrMsgTxt("The tensor factors must be of class DOUBLE."); 
    }
    if(mxIsComplex(pU1) || mxIsComplex(pU2) || mxIsComplex(pU3)) {
        mexErrMsgTxt("The tensor factors must be REAL."); 
    }
    if(!mxIsUint32(pI) || !mxIsUint32(pJ) || !mxIsUint32(pK)) {
        mexErrMsgTxt("I, J and K must be of class UINT32."); 
    }
    
    /* Get and check the dimensions of input arguments */ 
    n1 = mxGetM(pU1);
    n2 = mxGetM(pU2);
    n3 = mxGetM(pU3);
    r = mxGetN(pU1);
    nnz = mxGetM(pI);
    if(mxGetN(pU2) != r || mxGetN(pU3) != r)
        mexErrMsgTxt("Matrix dimensions mismatch for U1, U2 and U3.");
    if(mxGetM(pJ) != nnz || mxGetN(pJ) != 1)
        mexErrMsgTxt("Matrix dimensions mismatch for I and J.");
    if(mxGetM(pK) != nnz || mxGetN(pK) != 1)
        mexErrMsgTxt("Matrix dimensions mismatch for I and K.");
     
    /* Get pointers to the data in S and U */
    S = mxGetPr(pS);
    U1 = mxGetPr(pU1);
    U2 = mxGetPr(pU2);
    U3 = mxGetPr(pU3);
    I = (uint32_T*) mxGetData(pI);
    J = (uint32_T*) mxGetData(pJ);
    K = (uint32_T*) mxGetData(pK);
    
    /* Create the n1-by-r matrix for the ouput argument */ 
    pA = mxCreateDoubleMatrix(n1, r, mxREAL);
    if(pA == NULL)
        mexErrMsgTxt("TSPMASKMULT: Could not allocate X. Out of memory?");
    A = mxGetPr(pA); // column-major same convention as a MATLAB matrix 
    
    for(int it = 0; it < nnz; ++it)
    {
        // Let S(i,j k) denote the it-th nonzero entry of S. 
        // A(i,l) = S(i, j, k) * U3(k, l) * U2(j, l), for l = 1,...,r. 
        for(int l = 0; l < r; ++l)
        {
            A[I[it]-1+l*n1] += S[it] * U3[K[it]-1 + l*n3] * U2[J[it]-1 + l*n2];        
        }
    }
    return;
}
