function [arr, tab] = gen_hps_onerk(varargin)
% Generate a set of parameters such that the regularization parameters are on
% the grid of a given range of values. 
% 
% INPUT
%  varargin:  Input arguments in the form of the pair ('keyword', values). 
%             Valide keywords include: 
%             "n_hps"           Number of parameter configs to generate  
%             "pb_paramd"       An 2x3 array
%             "pb_rank"         An 2x1 array 
%              
% OUTPUT 
%  arr:       An 1D array of structs, each struct containing the following
%             fields
%  tab:       A table converted from "arr" presenting all the fields of each
%             of the structs of arr.
% 
% Example:
% ---------------------------------------------------------------
% >> [arr, tab] = gen_hps_onerk('n_hps', 4, 'pb_paramd', [-4, -4, -4; 2/3, 2/3,2/3], 'pb_rank', 5); 
% >> 
% tab =
%   4x2 table
%                   paramd                   rank
%     ___________________________________    ____
% 
%        0.0001       0.0001       0.0001     5
%     0.0035938    0.0035938    0.0035938     5
%       0.12915      0.12915      0.12915     5
%        4.6416       4.6416       4.6416     5
% 
% ---------------------------------------------------------------
% 
% Shuyu Dong, ICTEAM, UCLouvain 
% Latest version: March, 2020
% Contact: shuyu.dong@uclouvain.be 

opts   = parseArgs('tester', varargin{:});  
% paramd: [a1 b1 c1; a2 b2 c2]
% rank: [r1; r2]

% Generate Nhp parameter settings 
Nhp = opts.n_hps;  

% Example default rule for paramd's first coefficient: 
% generate (Nhp-1) uniformly distributed values between log(a1) and log(a2).
% Then return the exponents of these values. There is one setting reserved
% for paramd being all zeros. 

if opts.do_randomsearch(1)
    vals = 10.^(gen_RSddimSamples(log10(opts.pb_paramd), Nhp));
else
    vals = repmat(logspace(opts.pb_paramd(1,1), opts.pb_paramd(2,1), Nhp)', [1, 3]);
end
% val0 = zeros(size(vals(1,:))); 
% vals = [val0; vals]; 

vals2 = opts.pb_rank(1); 

% vals is a N1-by-3 array; vals2 is a number 

for i = 1 : size(vals, 1)
    arr(i)= struct('paramd', vals(i,:), 'rank', vals2); 
end

% Convert arr into a table to facilitate viewing/checking it.
tab = struct2table(arr); 

function s2dmat = gen_RSddimSamples(intervals, ns)
% INPUT:
%      intervals:   A 2D array of size, each column
%                   contains [a_min; a_max].
%      ns:          Number of samples to generate
% OUTPUT:
%      s2dmat:      A 2D array of size ns x d. 
    s2dmat = zeros(ns, size(intervals,2));
    dims_box = [-1, 1]*intervals;
    % Exclude NANs, which only happens when intervals has two Inf's
    % (log of zeros) 
    dims_box(isnan(dims_box)) = 0;        

    if ns > 2 
        for j  = 1 : size(intervals, 2)
            s2dmat(:,j) = intervals(1,j)+rand(ns,1)*dims_box(j) ; 
        end
    else
        s2dmat = intervals;
    end
end

end





