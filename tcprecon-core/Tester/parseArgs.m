function params = parseArgs(param_name, varargin)
% Input argument parser. 
% param_name = {'runTester', 'gdat', 'opt', 'pb'}.
% README to be updated. 
% 
% Based on a member function of GRMC's class Tester for experiments. 
% Contact: Shuyu Dong (shuyu.dong@uclouvain.be), ICTEAM, UCLouvain.
% Latest version: March, 2020.

DEFAULT_gdat = struct('gdatSizes', [300,300],...
                    'gdatType_g', 'community',... 
                    'gdatParam_g', nan,...
                    'gdatType_Z', 'graph-lowrank',... 
                    'gdatType_specfun', 'tikhonov',... 
                    'gdatType_specfun_doTruncate', false, ...
                    'gdatParam_specfun',1,...
                    'gdatParam_targetKappa', 100,...
                    'gdatParam_scale', 0.1,...
                    'gdatRank', [10, 10, 10],...
                    'gdatNoise', [0, 0]);
DEFAULT_tester    = struct('do_randomsearch',    [0,0],...
                            'altmin_tol_cg',  1e-9, ...
                                'n_hps',      10,...
                                'pb_paramd', zeros(2, 3),...
                                'pb_rank', [10;10], ...
                                'km16_tuckerrank', [10,10,10],...
                                'precon_delta_rgrad', 0, ... 
                                'repeatTests_maxiter', 800,...
                                'repeatTests_tolGrad', 1e-14,...
                                'n_repeatTests',    10); 


p = LRTC.build_parser(DEFAULT_gdat);
p = LRTC.build_parser(DEFAULT_tester, p);    

p = LRTC.build_parser(LRTC.default_pb, p);
p = LRTC.build_parser(Solver.DEFAULT_OPT, p);

parse(p, varargin{:});

switch param_name
    case 'pb'
        fds = fieldnames(LRTC.default_pb);
    case 'opt'
        fds = fieldnames(Solver.DEFAULT_OPT);
    otherwise
        fds = fieldnames(eval(sprintf('DEFAULT_%s', param_name)));
end
for i = 1 : length(fds)
	params.(fds{i}) = p.Results.(fds{i});
end
end
