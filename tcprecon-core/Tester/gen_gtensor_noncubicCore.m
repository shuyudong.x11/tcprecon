function data = gen_gtensor(varargin)
% Generate a tensor with a given Tucker rank. The data contains potentially
% noise. By default, the tensor is given by a 3-mode product 
%
% T = G x_1 U^{(1)} x_2 U^{(2)} x_3 U^{(3)} + Noise, 
% 
% where G is a r1-by-r2-by-r3 core tensor. Question: Under what condition
% (on G) is the Tucker rank of T equal to (r1, r2, r3)? 
% 
% Based on a member function of GRMC's class Tester for experiments. 
% Contact: Shuyu Dong (shuyu.dong@uclouvain.be), ICTEAM, UCLouvain.
% Latest version: March, 2020.

params_data = parseArgs('gdat', varargin{:});

G = gengraph_fromGSP(params_data.gdatType_g,...
                         params_data.gdatSizes(1), ...
                         params_data.gdatParam_g);
% targetKappa is an option in gdatParam_specfun(). 
[U, specValues, param] = gdata_specFilter(G, params_data);

% In case where the "param_specfun" is determined by the 
% target condition number "param_targetKappa":
params_data.gdatParam_specfun(end+1) = param; 

n23 = params_data.gdatSizes(2)*params_data.gdatSizes(3);    

% First generate the mode-1 matricization of the tensor using the 
% formula: T_{(1)} = U^{(1)} G_{(1)} (U^{(3)}\kron U^{(2)})^{T}. 
rank_data = params_data.gdatRank;
Core1 = randn(rank_data(1), rank_data(2)*rank_data(3)); 

switch params_data.gdatType_Z
    case 'graph-agnostic'

        U2 = randn(params_data.gdatSizes(2), rank_data(2));
        U3 = randn(params_data.gdatSizes(3), rank_data(3));
        G0 = randn(G.N, rank_data(1)); 
        % H0 = khatrirao(U3,U2);         
        H0 = kron(U3, U2);     
        Z  = G0*Core1*H0';
    case 'graph-fullrank'
        rank_data = NaN; 
        M = randn(G.N, n23); 
        Z = U * diag(specValues) * M;
        G0 = nan; H0 = nan;
	case 'graph-lowrank'
    % As in [Rao et al. '15], model with Z=AMB^T, where M has a
    % low rank. Generate a low-rank random Gaussian matrix M=
    % P\Sigma Q^T. 
        U2 = randn(params_data.gdatSizes(2), rank_data(2));
        U3 = randn(params_data.gdatSizes(3), rank_data(3));

        G0 = U *diag(specValues)* randn(G.N, rank_data(1)); 
        % H0 = khatrirao(U3,U2);              
        H0 = kron(U3, U2);              
        Z  = G0*Core1*H0';    
    otherwise
		fprintf('Warning: no matching type for gdatType_Z\n');
		return;
end
scale = params_data.gdatParam_scale; 
fa = (scale/ mean(abs(Z(:)))) ; % |Z_{ij}| ~ scale  
Z = Z * fa ; 
G0 = G0*sqrt(fa);
H0 = H0*sqrt(fa);
if ~isfield(params_data, 'gdatNoise') % ~.gdatNoise = (1/0, sigma)
    params_data.gdatNoise = [1, 1e-2];
end
if params_data.gdatNoise(2) > 1 
    % This is the case where the noise parameter is in
    % SNR(dB).
    SNRdB = params_data.gdatNoise(2);
    noise_sigma = norm(Z,'fro')/sqrt(prod(size(Z)))/10^(SNRdB/20);
else
    noise_sigma = params_data.gdatNoise(2);
end
if params_data.gdatNoise(1)
    Z_clean = Z;
    T_clean = tensor(reshape(Z_clean, params_data.gdatSizes));
else
    Z_clean = nan;
    T_clean = nan; 
end
Z = Z + params_data.gdatNoise(1)*noise_sigma*...
		                  randn(G.N, n23);

% Create the tensor using the class in tensor.m (could we just use the built-in
% k-way tensor instead of the tensor-toolbox's tensor.m class?)
T = tensor(reshape(Z, params_data.gdatSizes));

data = struct('name', sprintf('synthetic-%s',G.type), 'tensor', T, ...
             'T_clean', T_clean, ...
             'G0', G0, 'H0', H0,... 
             'dims', size(T), ...
             'tuckerrank', rank_data, ...  
             'norm_frob2', sum(Z(:).^2), ...
             'graph', G, 'gen_opts', params_data);
end
