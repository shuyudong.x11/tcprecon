function arr_params = get_crossval_params(RKs, filename)

if nargin < 2 

    % Encode directly according to the cv results obtained
    % +------------------------------------------------------------+ 
    %               paramd              rank
    %     __________________________    ____
    % 
    %     4.6416    4.6416    4.6416      5
    %         10        10        10     10
    %     21.544    21.544    21.544     15
    % +------------------------------------------------------------+ 
    RKS_TESTED = [5, 10, 15]; 
    PARAMS_CV  = [4.6416, 4.6416,4.6416; 10, 10, 10; 21.544, 21.544, 21.544];

    for i = 1 : numel(RKs)
        rk = RKs(i); 
        [~, iarg] = min(abs(rk-RKS_TESTED)); 

        arr_params(i).rank = rk;
        arr_params(i).paramd = PARAMS_CV(iarg(1), :); 
    end
else
    cvres = load(filename); 
    arr_params = cvres.hpsel; 
end


