function data = gen_tensor_low_tcrank(varargin)
% Generate a tensor for a given (low) Tucker rank. The tensor model is composed
% of a tensor with the given Tucker rank and additve noises. In the case of
% noiseless observations, the additive noises reduce to zeros. 
% 
% Example 1: the "clean, low-rank" part has a non-cubic core tensor: 
% data = gen_tensor_low_tcrank('gdatRank', [10, 12, 20],...
%                              'gdatSizes', [100, 100, 100],...
%                              'gdatNoise', [0, NaN]);
%  
% Example 2: the "clean, low-rank" part has a cubic core tensor: 
% data = gen_tensor_low_tcrank('gdatRank', [10, 10, 10],...
%                              'gdatSizes', [100, 100, 100],...
%                              'gdatNoise', [0, NaN]);
% 
% Example 3: observations with additive noises
% data = gen_tensor_low_tcrank('gdatRank', [10, 12, 20],...
%                              'gdatSizes', [100, 100, 100],...
%                              'gdatNoise', [1, 30]);
% 
% INPUT
%  varargin:  Optional input arguments in the form of the pair ('keyword',
%             values). Valide keywords include 
%               "gdatRank"          The Tucker rank of the low-rank part of the
%                                   tensor model. 
%               "gdatSizes"         The dimensions of the k modes of the
%                                   tensor
%               "gdatParam_scale"   The expected absolute value of the entries
%                                   of low-rank part of the tensor. It is 1 by default.
%               "gdatNoise"         A 1D array containing the boolean value for
%                                   whether the additive noise is applied and the
%                                   noise level in SNR if the additive noise is
%                                   to be applied. 
% OUTPUT 
%  data:      A structcontaining the following fields: such as 
%             - tensor     The generated tensor. 
%             - T_clean    The low-rank part of the tensor, when additive
%                          noises are applied. 
%
% Contact: Shuyu Dong (shuyu.dong@uclouvain.be), ICTEAM, UCLouvain.
% Latest version: July, 2020.
 
if isempty(varargin)
    params_data = parseArgs('gdat', 'gdatRank', [10, 12, 20], 'gdatSizes',...
    [100, 100, 100], 'gdatNoise', [0, NaN]);
else
    params_data = parseArgs('gdat', varargin{:});
end
rank_tc = params_data.gdatRank;     % Tucker rank of the low-rank part 
tsize   = params_data.gdatSizes;    % Size of the tensor. An 1-by-k array for a kth-order tensor
sigma_T = 1;                        % Standard deviation of the entries of Tclean
SCALE   = params_data.gdatParam_scale; 

%% Generate the low-rank part with Gaussian entries ~ N(0, sigma_T)
T = tensor(sigma_T*randn(tsize));

%% Tucker rank truncation 
% Truncation by Tucker decomposition (Tensortoolbox):
% https://www.tensortoolbox.org/tucker_als_doc.html.
% The result Z has a Tucker rank given by rank_tc. 
fprintf('Generate a tensor with a prescribed Tucker rank (%s):\n',...
num2str(rank_tc)); 
Z = tucker_als(T, rank_tc, 'tol', 1e-5,'printitn', 100); 

%% Rescale the entries of Z to the target value
fac = SCALE/(norm(Z)/sqrt(prod(tsize))); 
Z = Z * fac;
% Now the expected absolute value of the entries of Z equals SCALE.

%% Additive noise 
if ~isfield(params_data, 'gdatNoise') % 
    params_data.gdatNoise = [1, 1e-2];
end
if params_data.gdatNoise(1)
% Noisy observations: apply additive noise 
if params_data.gdatNoise(2) > 1 
    % This is the case where the noise parameter is in
    % SNR(dB).
    SNRdB = params_data.gdatNoise(2);

    % By construction (line 62-63), the expected absolute value of the entries
    % equals SCALE. The SNR in dB is defined as 
    % SNR = 10log10(SCALE^2/noise_sigma^2). 
    noise_sigma = SCALE / 10^(0.5*SNRdB/10);
     
    % The above line is equivalent to the old version: 
    % noise_sigma = norm(Z)/sqrt(prod(size(Z)))/10^(SNRdB/20);
else
    noise_sigma = params_data.gdatNoise(2);
end
    T_clean = Z;
    Z = tensor(T_clean) + tensor(noise_sigma* randn(tsize));
else
% Noiseless observations: leave Z as it is. 
    T_clean = NaN;
    Z = tensor(Z); 
end

fprintf('Generation of the synthetic tensor is finished.\n'); 
%% Tensor Model
data = struct('name', sprintf('synthetic-tcrank'), 'tensor', Z, ...
             'T_clean', T_clean, ...
             'dims', size(T), ...
             'tuckerrank', rank_tc, ...  
             'norm_frob2', norm(Z)^2, ...
             'gen_opts', params_data);
end

