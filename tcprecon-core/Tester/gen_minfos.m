function [output, tab, labels] = gen_minfos(mnames, varargin)
% Generate an array of structs, in which each struct contains the
% fields "manifold", "algname", "descriptors".
% INPUT: 
% mnames   A cell array of strings and/or index numbers. 
%          Note: A method name obey the following "space-separated" convention:
%          '<manifold name> <algorithm name> <descriptor>'.
%          
%          Example 1: mnames = {'Precon RGD (linemin)', ...
%                               'Precon RCG (linemin)',...
%                               'Euclidean GD (Armijo)',...
%                               'NA AltMin 1'}
%          Note: when plotting the method name in the legend, the string "NA" need not be printed. 
%          Example 2: mnames = {1, 3, 'Euclidean GD (Armijo)',...
%                               'NA AltMin 1'}
% OUTPUT: 
% minfos   An array of structs. 
%          Each struct contains the following fields:
%          - manifold: the manifold structure if applicable. 
%          - algname: the algorithm name. 
%          - descriptor: all other keywords for detailed features, eg. line-search rule. 
%          - label: the name that represents the method. 
% The parser used to split the given method names is "strsplit". ,
% Based on a member function of GRMC's class Tester for experiments. 
% Contact: Shuyu Dong (shuyu.dong@uclouvain.be), ICTEAM, UCLouvain.
% Latest version: March, 2020.
MLIST = {'Precon RGD linemin';...
         'Precon RGD Armijo';...
         'Precon RCG linemin';...
         'Precon RCG Armijo';...
         'Precon RGD BB';...
         'Euclidean GD linemin';...
         'Euclidean GD Armijo';...
         'Euclidean CG linemin';...
         'Euclidean CG Armijo';...
         'Euclidean GD BB';...
         'KM16 KM16 eqrks';...
         'AltMin AltMin NA'; 'INDAFAC INDAFAC NA';...
         'CP-WOPT CP-WOPT NA'; 'FaLRTC FaLRTC NA';...
         'SiLRTC SiLRTC NA'; 'HaLRTC HaLRTC NA';...
         'KM16 KM16 rstar';'TNCP TNCP NA';...
         'airCP airCP NA';...
         'Precon RGD BB1';...
         'Precon RGD BB2';...
         'Precon RGD rBB';...
         'Precon RGD rBB1';...
         'Precon RGD rBB2';...
         'Precon_mex RGD linemin';...
         'Euclidean_mex RGD linemin'...
         }; 
if nargin < 1
    mnames = MLIST; % {'Precon RGD (linemin)'}; 
end
if isempty(mnames)
    mnames = MLIST;
end
for i = 1 : numel(mnames)
    tmp = mnames{i}; 
    if ischar(tmp) 
        % This is the case where tmp is a string composed of more
        % than one identifier (eg. "Precon RGD (linemin)"). 
        mname = tmp; 
    elseif isnumeric(tmp)
        % This is the case where tmp is an index number of the method
        % name in the default list MLIST. 
        mname = MLIST{tmp}; 
    end
    names   = strsplit(mname); 
    if strcmp(names{1}, names{2})
        % This only applies to external methods. 
        if strcmp(names{3}, 'NA')
            mlabel = sprintf('%s', names{2});
        else
            mlabel = sprintf('%s (%s)', names{2}, names{3});
        end
        param = nan; 
    else
        % This only applies to methods implmented within @Solver.     
        mlabel = sprintf('%s %s (%s)', names{1}, names{2}, names{3}); 
        param = 0; 
    end
    % labels{i,1} = mlabel; 
    im(i) = struct('manifold', names{1}, 'algname', names{2}, ...
               'descriptor', names{3}, 'name', mlabel,...
               'precon_delta', param);
end

% Read optional settings for the optimization-related parameters of 
% the algorithms. A priori, only our proposed algorithms are concerned with
% optional input parameters. valid input argument is 
% "opt_delta_rgrad", an array of values
opts = parseArgs('tester', varargin{:});
vals_delta = opts.precon_delta_rgrad;  
% todo: Now the question is to insert additional mname's into the list
% "im". We append these additional mname's at the end of the list.  
% The methods concerned are those whose manifold is Precon 
nn = 0; 
iis = []; 

for ii = 1 : numel(im)
    if strcmp(im(ii).manifold, 'Precon')
        iis(end+1) = ii; 
        for tt = 1 : numel(vals_delta)
            if vals_delta(tt) > 0 
                nn = nn+1; 
                mlabel = sprintf('%s %s (%s)', 'Precon', ...
                                 im(ii).algname, im(ii).descriptor); 
                param = vals_delta(tt); 
                % labels{end+1,1} = mlabel; 
                imm(nn) = struct('manifold', 'Precon', 'algname',...
                                    im(ii).algname, 'descriptor', ...
                                    im(ii).descriptor, 'name', ...
                                    mlabel, 'precon_delta', param);
            end
        end
    end
end
% Append imm at the begining of im 
% output = [imm, im]; 
%% Remove Precon * (delta=0) and then append imm (Precon * delta > 0)
iretain = setdiff([1:numel(im)], iis); 
output = im(iretain); 
if exist('imm','var')
    output = [imm, output]; 
end

tab = struct2table(output); 
labels = table2cell(tab(:,'name')); 

end

