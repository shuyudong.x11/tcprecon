function [param_sel, cvinfo] = tc_paramsel(pb, alg, arr_ps, SR, tid, varargin)
% function param_sel = tc_paramsel(tc_data, alg, arr_ps, SR, tid, varargin)
% 
% INPUT
%       tc_data  :  The input data tensor containing all the available data
%       arr_ps   :  An N-by-4 array, the 4 columns contain (1D) rank and
%                   a 3D parameter.
%       tid      :  An id number of the global experiment, eg.,
%                   numerical form of the start time of the experiment.
% OUTPUT
%       param_sel:  An Nr-by-1 array of structs extracted from the input
%                   arr_ps, where Nr is the number of different rank choices. 
% 
% Latest version: June 2020.
% Contact: shuyu.dong@uclouvain.be

% The main purpose of this function is to find out the best regularization
% parameter for each group of algorithms. The groups considered are
% 
%   CPD_GD:       Gradient-based algorithms, eg., RGD, RCG, Euclidean GD/CG for the CPD problem 
%   CPD_AltMin:   AltMin for the CPD problem.
%   KM16:         the Tucker decomposition-based RCG algorithm. 


opts = parseArgs('opt', varargin{:});
tcinfo0 = pb.tcinfo; 
%% Retrive some facts about arr_ps
RKs = unique([arr_ps.rank]);

%% Cross validation setting: split the data in K parts
K = 3; 
tcinfos = cv_split(tcinfo0, K);

switch alg
case 'CPD_GD'
    % Use Qprecon RCG as the algorithm for testing
    minfo = gen_minfos({'Precon RCG linemin'}, 'precon_delta_rgrad', 1e-7);
case 'CPD_AltMin'
    minfo = gen_minfos({'AltMin AltMin reg'});
case 'KM16'
    minfo = gen_minfos({'KM16 KM16-reg eqrks'});
end

nruns = K*numel(arr_ps) ; 
fprintf('The algorithm solver will run for %d times in total (Kfold CV of all parameter configs)...\n', nruns); 
pause(3); 

irun = 0; 
ip = 0; 
IS_CV = true; 
irun = 0; 
% Get the best score for each rank choice 
for j = 1 : numel(RKs)
    subs = find([arr_ps.rank]==RKs(j));
    Xinit = LRTC.initialize_cpd(tcinfo0, RKs(j), opts.method_init); 
    pb.set_params_pb('rank', RKs(j)); 

    clearvars stoppt scmean_tr scmean_val; 
    % Get the scores of stoppts at all params at the given rank  
    for i = 1 : numel(subs)
        % Get the cv score of the i-th parameter config 
        ip = ip+1; 
        for k = 1 : K
            irun = irun + 1; 
            pb.set_params_pb('paramd', arr_ps(subs(i)).paramd);
            pb.refresh_obj([], tcinfos(k), []); 

            fprintf('======= Kfold CV of the parameter config...%d/%d: \n', irun, nruns); 
            disp(struct2table(pb.params_pb)) ;
             
            [~, stats, rinfo] = runmethods2_fromlist(pb, Xinit, minfo, subs(i), k, ...
                                                   varargin{:}); 
            stoppt(i,k) = stats(end);
            % stoppt(i,k) = struct('RMSE_tr', rand, 'RMSE_t',rand); 
            % rinfo = [struct2table(pb.params_pb), struct2table(minfo)];
        end
        % Report all scores 
        scmean_tr(i) = mean([stoppt(i,:).RMSE_tr],2); 
        scmean_val(i) = mean([stoppt(i,:).RMSE_t],2); 
        tmp = [struct2table(struct('iHP', subs(i), 'RMSE_tr', scmean_tr(i),...
                                        'RMSE_val', scmean_val(i))), rinfo] 
        cvinfo(ip) = table2struct(tmp); 
    end
    % scores = [stoppt.RMSE_t]; % scores is a Nhp-by-K array
    [~, iarg] = min(scmean_val); % iarg is an index of "subs"
    param_sel(j) = arr_ps(subs(iarg));
end


end

