function data = load_tensor(dataname, varargin)
% Load external data sets and form a unified input data struct. 
% Contact: Shuyu Dong (shuyu.dong@uclouvain.be), ICTEAM, UCLouvain.
% Latest version: Mai, 2020.

if nargin < 1 
    dataname = 'ML100k';
end

switch dataname
case {'giant'} 
    %% load data (you can change the file addres in this sentence.)
    %addres = 'giant.png';
    imgname = sprintf('%s.png', dataname); 
    T   = double(imread(imgname));
    T   = tensor(T);
case {'ribeira'}
    % Note: it is better to download it from the website using "wget" for
    % example. 
    temp    = load('datasets/ref_ribeira.mat'); 
    T       = tensor(temp.reflectances); 
case {'lena'} 
    %% load data (you can change the file addres in this sentence.)
    %addres = 'lena.bmp';
    %addres = 'giant.png';
    imgname = sprintf('%s.bmp', dataname); 
    T   = double(imread(imgname));
    T   = tensor(T);
case 'FIA'
    raw  = loaddata('fia');
    dims = [12, 100, 89];
    T   = sptensor(reshape(raw.X, dims));
case 'ML1M'
    raw = importdata('datasets/ml-1m/ratings.dat'); 
    % The loaded data "raw" is an array of size 100k x 4. The first two column
    % contain the movie and the user ids and the third column contains the
    % rating; the fourth column contains the time stamp.  
    dims = [6040  3952  150];
    ind3 = floor((raw(:,4)-datenum(1970,1,1))/604800)-1579;
    subs = [raw(:,1), raw(:,2), ind3];
    vals = raw(:,3);
    T = sptensor(subs, vals, dims) ;
case 'ML100k'
    raw = load('u.data');
    % The loaded data "raw" is an array of size 100k x 4. The first two column
    % contain the movie and the user ids and the third column contains the
    % rating; the fourth column contains the time stamp.  
    dims(1) = numel(unique(raw(:,1))); 
    dims(2) = numel(unique(raw(:,2))); 
    dims(3) = 7; % the third dimension is the time organized in weekdays. 

    subs = [raw(:,1), raw(:,2), weekday(raw(:,4))];
    vals = raw(:,3);
    T = sptensor(subs, vals, dims) ;
otherwise 
    error(sprintf('The dataset %s is not available.', dataname)); 
end


data = struct('name', dataname, 'tensor', T, ...
              'dims', size(T));



end

