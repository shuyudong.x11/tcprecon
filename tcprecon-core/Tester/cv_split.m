function tcinfos = cv_split(tc_data, Kcv)
% INPUT:
%       T:         The possibly sparse tensor containing all the available data
% OUTPUT:   
%       tcinfos     A 1D array of structs, each element contains a tcinfo. 
%                   - tcinfo: a struct containing (I, J, K, val, size)
% 
% Latest version: June 2020.
% Contact: shuyu.dong@uclouvain.be

% Split the set of known entries into two parts: a training set (I, J, K, Ttr) and a
% validation set (Ite, Jte, Kte, Tte).  

% Get some facts about the input data that is available to the algorithm. 
SR   = tc_data.SR; 
dims = tc_data.sz_tensor; 
sz   = numel(tc_data.I); 

% Common choices of Kcv are 3 or 5, for 3-fold or 5-fold CV. Limit the number of folds under 9.  
Kcv = min(Kcv, 9); 

% The size of the validation set should not change too much the sampling rate of
% the tc problem, even if this is a cross validation step. We generate Kcv
% validation sets in Kcv complementary parts of the input data. The size of
% validation set is of course smaller than one Kcv-th the total size. 

sz_val  = ceil((1/10)*sz); % 1/10 is surely smaller than 1/Kcv 
inds = randperm(sz); 

eind = 0; 
k = 1; 
while eind < sz  
    % Get the k-th complementary part 
    hind = eind+1; 
    eind = min(sz, ceil(k*sz/Kcv)); 

    % Generate validation set in the k-th part and get its complementary set as
    % the training set 
    subs = randsample([hind:eind], sz_val); 
    subs_c = setdiff([1:sz], subs); 

    Ite = tc_data.I(inds(subs)); 
    Jte = tc_data.J(inds(subs)); 
    Kte = tc_data.K(inds(subs)); 
    Tte = tc_data.Ttr(inds(subs)); 

    I = tc_data.I(inds(subs_c)); 
    J = tc_data.J(inds(subs_c)); 
    K = tc_data.K(inds(subs_c)); 
    Ttr = tc_data.Ttr(inds(subs_c)); 
    SR_eff = numel(I)/(tc_data.sz_tr/SR); 
    % Form the tcinfo struct 
    trans = [];
    Omega = [I,J,K];
    for i = 1:3
        bte{i} = setdiff(1:3,i);
        sz_prodneqi(i) = prod(dims(bte{i}));
        Omega_temp = Omega(:,[bte{i},i]);
       for j = 1: numel(I)
           trans(j,i) = Omega_temp(j,1) + (Omega_temp(j,2)-1)*dims(bte{i}(1));
       end
    end
    Omega_mat{1} = [trans(:,1),I]; 
    Omega_mat{2} = [trans(:,2),J];
    Omega_mat{3} = [trans(:,3),K]; 
    unames = {'u1','u2','u3'};
    tcinfos(k) = struct('I', uint32(I), 'J', uint32(J), 'K', uint32(K),...
                    'Ite', uint32(Ite), 'Jte', uint32(Jte), 'Kte', uint32(Kte),...    
                    'Ttr', Ttr, 'Tte', Tte,...
                    'Ite_comp', uint32(Ite), 'Jte_comp', uint32(Jte), 'Kte_comp', uint32(Kte),...    
                    'Tte_comp', Tte,...
                    'SR',  SR_eff, ... 
                    'sz_tr', numel(I),'sz_te',numel(Ite),...
                    'sz_te_comp', numel(Ite), ... 
                    'sz_tensor', dims, 'sz_prodneqi', sz_prodneqi, ... 
                    'ind_neqi', {bte}, ...
                    'unames', {unames}, ...
                    'Omega_mat', {Omega_mat}); 
    k = k + 1; 
end

end

