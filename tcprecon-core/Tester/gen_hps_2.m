function [arr, tab] = gen_hps_2(varargin)
% Generate a set of parameters. 
% INPUT
%  varargin:  Optional input arguments in the form of the pair ('keyword',
%             values). Valide keywords include "pb_paramd",
%             "pb_rank", "n_hps"; see Tester/parseArgs.m,
%             DEFAULT_tester. 
% 
% OUTPUT 
%  arr:       An 1D array of structs, each struct containing the following
%             fields
%             - paramd: a 1-by-d array composed of regularization
%                       parameters of the problem,
%             - rank: the CPD rank of the candidate tensor. 
%  tab:       A table converted from "arr" presenting all the fields of each
%             of the structs of arr.
%
% Based on a member function of GRMC's class Tester for experiments. 
% Contact: Shuyu Dong (shuyu.dong@uclouvain.be), ICTEAM, UCLouvain.
% Latest version: March, 2020.

opts   = parseArgs('tester', varargin{:});  
% paramd: [a1 b1 c1; a2 b2 c2]
% rank: [r1; r2]

% Generate Nhp parameter settings 
Nhp = opts.n_hps;  

% Example default rule for paramd's first coefficient: 
% generate (Nhp-1) uniformly distributed values between log(a1) and log(a2).
% Then return the exponents of these values. There is one setting reserved
% for paramd being all zeros. 

% if any(opts.pb_paramd < 0)
%     error('The values in paramd must be nonnegative.');
% end
if opts.do_randomsearch(1)
    vals = 10.^(gen_RSddimSamples(log10(opts.pb_paramd), Nhp));
else
    vals = repmat(logspace(opts.pb_paramd(1,1), opts.pb_paramd(2,1), Nhp)', [1, 3]);
end
if opts.do_randomsearch(2)
    vals2 = ceil(gen_RSddimSamples(opts.pb_rank, Nhp));
else
    if numel(opts.pb_rank) > 2
        vals2 = opts.pb_rank;
    else
        vals2 = ceil(linspace(opts.pb_rank(1), opts.pb_rank(2), Nhp));
    end
end

% Reserve the first one with paramd being all zeros. 
arr(1) = struct('paramd', zeros(1,3),  'rank', opts.pb_rank(1)); 
tmp(1,:) = [arr(1).paramd, arr(1).rank]; 
if Nhp > 1 
    % Fill out the rest of the table (tmp) and arr. 
    for i = 2 : Nhp
        tmp(i,:) = [vals(i,:), vals2(i)]; 
        arr(i)= struct('paramd', vals(i,:), 'rank', vals2(i)); 
    end
end
% In case all the intervals given are actually singletons, exclude 
% repeated rows (parameter settings) and take only one of them. 
[~, ia, ~] = unique(tmp, 'rows'); 
arr = arr(ia); 

% Convert arr into a table to facilitate viewing/checking it.
tab = struct2table(arr); 

function s2dmat = gen_RSddimSamples(intervals, ns)
% INPUT:
%      intervals:   A 2D array of size, each column
%                   contains [a_min; a_max].
%      ns:          Number of samples to generate
% OUTPUT:
%      s2dmat:      A 2D array of size ns x d. 
    s2dmat = zeros(ns, size(intervals,2));
    dims_box = [-1, 1]*intervals;
    % Exclude NANs, which only happens when intervals has two Inf's
    % (log of zeros) 
    dims_box(isnan(dims_box)) = 0;        

    if ns > 2 
        for j  = 1 : size(intervals, 2)
            s2dmat(:,j) = intervals(1,j)+rand(ns,1)*dims_box(j) ; 
        end
    else
        s2dmat = intervals;
    end
end

end



