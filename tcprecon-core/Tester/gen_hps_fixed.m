function [arr, tab] = gen_hps_fixed(paramds, rks)
% Generate a set of parameters. 
% INPUT
%  paramds:   An 2D array of size Nhp x 3 
%  rks        An 1D array of size Nhp x 1 
% 
% OUTPUT 
%  arr:       An 1D array of structs, each struct containing the following
%             fields
%             - paramd: a 1-by-d array composed of regularization
%                       parameters of the problem,
%             - rank: the CPD rank of the candidate tensor. 
%  tab:       A table converted from "arr" presenting all the fields of each
%             of the structs of arr.
%
% Based on a member function of GRMC's class Tester for experiments. 
% Contact: Shuyu Dong (shuyu.dong@uclouvain.be), ICTEAM, UCLouvain.
% Latest version: March, 2020.

Nhp = size(paramds,1);

for i = 1 : Nhp
    arr(i)= struct('paramd', paramds(i,:), 'rank', rks(i)); 
end

% Convert arr into a table to facilitate viewing/checking it.
tab = struct2table(arr); 

end




