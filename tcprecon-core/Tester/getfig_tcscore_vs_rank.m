function [curves, h] = getfig_tcscore_vs_rank(res, arr_hps, minfos, SRs, iSR, labely)
if nargin < 5
    iSR = 1;
end
if nargin < 6
    labely = 'RMSE';
end
switch labely
    case 'RMSE'
        sc_tr = 'RMSE_tr'; 
        sc_te = 'RMSE_t'; 
    % case 'relErr' % not available yet since it is not computed/stored at each iteration
    %     sc_tr = 'relErr_tr';
    %     sc_te = 'relErr_te';
    otherwise
        error('The score %s is not available in the stats...\n', labely); 
end
ploty = 'log';
plotx = 'lin'; 
% res is a 4D array of (SR, HP, methods, Repeat)

for j = 1  : numel(minfos)
    x = []; ytr = []; yte = []; 
    for rr = 1 : size(res, 2)
	    tmp = res{iSR, rr, j, 1}; 
	    x(rr) = arr_hps(rr).rank; 
	    tmptr = [tmp.(sc_tr)]; 
	    tmpte = [tmp.(sc_te)]; 
        ytr(rr) = tmptr(end); % get the score of the last iteration (the output point)
        yte(rr) = tmpte(end); % get the score of the last iteration (the output point)
    end
    curves(j) = struct('x', x, 'ytr', ytr, 'yte', yte, ...
                       'xlabel', 'R', ...
	    			   'ylabel', labely, 'SR', SRs(iSR), ...
	    			   'name_tr', sprintf('Train (%s)', minfos(j).name),...
	    			   'name_te', sprintf('Test (%s)', minfos(j).name),...
	    			   'plotx', plotx, 'ploty', ploty); 
end

legs = {};
h = figure();
fs = 12;

switch curves(1).ploty
case 'log'
	for i = 1 : numel(curves)
	    semilogy(curves(i).x, curves(i).ytr, '^-', 'linewidth', 2.0); hold on; 
	    semilogy(curves(i).x, curves(i).yte, 'o-.', 'linewidth', 2.0); hold on; 
	    legs{end+1} = curves(i).name_tr; 
	    legs{end+1} = curves(i).name_te; 
	end
case 'lin'
	for i = 1 : numel(curves)
	    plot(curves(i).x, curves(i).ytr, '^-', 'linewidth', 2.0); hold on; 
	    plot(curves(i).x, curves(i).yte, 'o-.', 'linewidth', 2.0); hold on; 
	    legs{end+1} = curves(i).name_tr; 
	    legs{end+1} = curves(i).name_te; 
	end
otherwise
	error('plot mode not available yet...\n');
end

pbaspect([1 1 1]);

ax1 = gca;
set(ax1,'FontSize',fs);
xlabel(ax1, curves(1).xlabel,'FontSize',fs);
ylabel(ax1, curves(1).ylabel,'FontSize',fs);
legend(legs, 'Location','northeast');
legend 'boxoff';
% box off;
% title(['Sampling rate: ',num2str(curves(1).SR)]); 

