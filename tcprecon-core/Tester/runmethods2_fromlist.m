function [X_, stats_, runinfo] = runmethods2_fromlist(pb, Xinit, minfo, Ihp, Ite, varargin)
% INPUT 
% minfo:  an 1d array of structs. Each struct contains the fields "manifold", "algname", "descriptor", "name". 
% 
% OUTPUT
% stats_:      An 1D array of structs, each struct contains the
%              properties of the iterate of a given algorithm. These
%              properties are:
%              - SR:      Sampling rate of the problem, this value certains
%                         repeats along the results of one single test. 
%              - paramspb:A string containing the regularization parameters, with num2str. 
%              - rank:    CP rank paramter of the problem model.
%              - iter:    Number of iteration.
%              - time:    Wall time from the intial time to the current
%                         point. 
%              - RMSE_tr: Training error of the iterate in terms of RMSE.
%              - RMSE_t:  Test error of the iterate in terms of RMSE.
%              - RMSE:    RMSE of the iterate (on all the known
%                         entries available).
%              - relErr:  Relative error of the iterate (on all the
%                         known entries available).


% Get current algorithm name  
if ~isempty(pb.tid)
    tid = pb.tid;
else
    tid = 0;
end
man         = minfo.manifold; 
algname     = minfo.algname; 
descriptor  = minfo.descriptor; 
opts = parseArgs('opt', varargin{:});
opts_extern = parseArgs('tester', varargin{:}); 

fprintf('>>>>>>>> Running method: %s...\n', minfo.name); 

switch algname
    case {'RGD', 'GD'}
        % The following set-up ensures that the parameter delta_rgrad 
        % be passed to the construction of the problem's function handle
        % for computing the gradient. Non-zero values of delta only apply 
        % to Qprecon algorithms; see the table of methods (second output of 
        % gen_minfos.m).   
        pb.set_params_pb('delta_rgrad', minfo.precon_delta);
        % Exception for BB-type algorithms, the stopping criterion is the
        % default one (time budget, tolgradnorm etc).  
        switch descriptor
            case {'BB', 'BB1','BB2','RBB1','RBB2'}
                pb.stopfun = @(optpb, X, info, last)LRTC.stopfun_manopt(optpb, X, info, last);
        end
        % Construct the solver from the problem object pb  
        so = Solver(pb, varargin{:});
        switch descriptor
            case 'Armijo'
                [X_, stats_]  = so.solve_rsd_manopt(Xinit);
            case 'linemin'
                [X_, stats_]  = so.solve_RGD_lsFree(Xinit);
            case 'BB'
                [X_, stats_]  = so.solve_RGD_lsBB(Xinit); 
            case 'BB1'
                [X_, stats_]  = so.solve_RGD_lsBB1(Xinit); 
            case 'BB2'
                [X_, stats_]  = so.solve_RGD_lsBB2(Xinit); 
            case 'RBB'
                [X_, stats_]  = so.solve_RGD_lsRBB(Xinit); 
            case 'RBB1'
                [X_, stats_]  = so.solve_RGD_lsRBB1(Xinit); 
            case 'RBB2'
                [X_, stats_]  = so.solve_RGD_lsRBB2(Xinit); 
            otherwise 
                error('The method %s is not available yet..\n', minfo.name); 
        end
    case {'RCG', 'CG'} 
        % The following set-up ensures that the parameter delta_rgrad 
        % be passed to the construction of the problem's function handle
        % for computing the gradient. Non-zero values of delta only apply 
        % to Qprecon algorithms; see the table of methods (second output of 
        % gen_minfos.m). 
        pb.set_params_pb('delta_rgrad', minfo.precon_delta); 
        
        % Construct the solver from the problem object pb 
        so = Solver(pb, varargin{:});
        switch descriptor
            case 'Armijo'
                [X_, stats_]  = so.solve_rcg_manopt(Xinit);
            case 'linemin'
                [X_, stats_]  = so.solve_RCG_lsFree(Xinit);
            otherwise 
                error('The method %s is not available yet..\n', minfo.name); 
        end
    case {'KM16', 'KM16-reg'} % the method name should be "KM16 (<descriptor>)" 
        [dtr, dte, dva] = conv_tcinfo_toKM16(pb.tcinfo); 
        %  Required problem description
        problem_description.data_train = dtr;
        problem_description.data_test = dte;
        % problem_description.data_validate = dva;
        problem_description.tensor_size = dtr.size; 
        
        tensor_dims = pb.tcinfo.sz_tensor; 
        switch descriptor 
            case {'eqrks', 'R,R,R'} 
                % Set the Tucker ranks as the same 
                core_dims = repmat(pb.params_pb.rank, [1, numel(dtr.size)]);
                core_dims = min([core_dims; tensor_dims], [], 1); 
                    % This changes core_dims only when there exists r in core_dims that exceeds a certain tensor size, e.g., when the core_dims = [10,10,10] and the tensor size is [900, 500, 7]. 
                problem_description.tensor_rank = core_dims; 
            case {'rstar', 'r-true'} 
                % Set the Tucker rank by the given values, opt's keyword is "km16_tuckerrank" 
                core_dims = opts_extern.km16_tuckerrank; 
                problem_description.tensor_rank = core_dims; 
            otherwise 
                if ~isempty(str2num(descriptor))
                    % This is the case where descriptor = 'r1, r2, r3', where ri's are integers. 
                    core_dims = str2num(descriptor);
                    problem_description.tensor_rank = core_dims;
                    Xinit = LRTC.initialize_cpd(pb.tcinfo, core_dims(1), 'random');  %initial_U_testtensor(tcinfo, opts);
                else
                    error('The method %s is not available yet..\n', minfo.name); 
                end
        end
        % Generate random initial point regardless of the input initial
        % point Xinit. 
        % X_init = makeRandTensor(tensor_dims, core_dims);
        % Special case: Get the tucker tensor of the initial point in terms of CPD: 
        X_init = get_ttensor_from_cpd(Xinit);

        problem_description.X_initial.U1 = X_init.U1;
        problem_description.X_initial.U2 = X_init.U2;
        problem_description.X_initial.U3 = X_init.U3;
        problem_description.X_initial.G = X_init.G; 
        problem_description.weights = ones(length(dtr.entries),1);

        % Some options, but not mandatory. Local options 
        tolmse = 0; % disable this tolerence param; this residual option is not needed given tolgradnorm below. 
        tolgradnorm = opts.tolgradnorm; %1e-8;
        maxiter = opts.maxiter; % 200;

        %% stopping criterion
        problem_description.params.tolgradnorm = tolgradnorm; 
        problem_description.params.tolmse = tolmse;
        problem_description.params.maxiter = maxiter;
        problem_description.params.maxtime = opts.maxtime;
        problem_description.params.tol_relchg_rmse = opts.tol_relchg_rmse;

        %% solver 
        problem_description.params.solver = @conjugategradient; % Conjugate gradients
        problem_description.params.linesearch = @linesearchguess2; % Stepsize guess with 2 polynomial
        if strcmp(algname, 'KM16-reg')
            problem_description.params.lambda = pb.params_pb.paramd(1); % Regularization.
        else
            problem_description.params.lambda = 0; % Regularization.
        end
        
        % Run alogorithm
        [X_, infos] = fixedrank_tensor_completion_customizedopts(problem_description);

        % infos has the exact same structure as stats, but do not have RMSE* entries. 
        stats_ = infos; 

        % Convert test error into RMSE on test entries
        for i = 1 : numel(stats_)
            if isfield(stats_(1), 'train_error_square')
                stats_(i).RMSE_tr =... 
                sqrt(2*stats_(i).train_error_square/problem_description.data_train.nentries); 
            end
            stats_(i).RMSE_t = sqrt(2*stats_(i).test_error_square/problem_description.data_test.nentries); 
        end
    % case 'AltMin_unreg'
    %     opts.U = Xinit;
    %     opts.R = pb.params_pb.rank;  %cpd_rank
    %     [out, stats_] = tc_altmin_nongraph(pb.tcinfo, opts);
    %     %optional input: opts.tol_cg, opts.maxit_cg to control the subproblem
    %     X_ = out.U; 
    case 'AltMin'
        opts.U = Xinit;
        opts.R = pb.params_pb.rank;  %cpd_rank
        if ~isfield(opts, 'tol_cg')
            opts.tol_cg = opts_extern.altmin_tol_cg; 
        end
        if strcmp(descriptor, 'reg')
            opts.Lambda = pb.params_pb.paramd;  % reg parameters 
            % [out, stats_] = altmincg_reg_frob(pb.tcinfo, opts);
            [out, stats_] = tc_altmin_reg(pb.tcinfo, opts);
        else
            % optional input: opts.tol_cg, opts.maxit_cg to control the subproblem
            [out, stats_] = tc_altmin_nongraph(pb.tcinfo, opts);
        end
        X_ = out.U; 
    case 'airCP'
        opts.U = Xinit;
        opts.R = pb.params_pb.rank;  %cpd_rank
        opts.alpha_airCP = pb.params_pb.paramd(1:2);  %cpd_rank
        [out, stats_] = tc_AirCP(pb.tcinfo, opts);
        X_ = out.U; 
    case 'TNCP'
        opts.U = Xinit;
        opts.R = pb.params_pb.rank;  %cpd_rank
        opts.alpha_TNCP = pb.params_pb.paramd;  %cpd_rank
        [out, stats_] = tc_TNCP(pb.tcinfo, opts);
        X_ = out.U; 
    case 'INDAFAC'
        opts.U = Xinit;
        opts.R = pb.params_pb.rank;  %cpd_rank
        [out, stats_] = INDAFAC(pb.Tstar, pb.tcinfo, opts);
        X_ = out.U; 
    case 'CP-WOPT'
        % not ready 
        opts.U = Xinit;
        opts.R = pb.params_pb.rank;  %cpd_rank
        [out, stats_] = cp_wopt_2(pb.tcinfo, opts, 'alg','ncg_2','alg_options', opts);
        X_ = out.U; 
    case 'FaLRTC'
        % Note (19 Apr): the results are suspicious. 
        [out, stats_] = FaLRTC1(pb.Tstar, pb.tcinfo, opts);
        X_ = out.Y; 
    case 'SiLRTC'
        % Note (19 Apr): the results are suspicious.
        [out, stats_] = SiLRTC1(pb.Tstar, pb.tcinfo, opts);
        X_ = out.X; 
    case 'HaLRTC'
        % Note (19 Apr): the results are suspicious.
        [out, stats_] = HaLRTC1(pb.Tstar, pb.tcinfo, opts);
        X_ = out.X; 
    otherwise 
        error('The method %s is not available yet..\n', minfo.name); 
end

%% Collect all information of this test (problem parameter, method info, opt parameter (eg. delta_rgrad))
runinfo = [struct2table(pb.params_pb), struct2table(minfo)];

%% Append problem info into the results per iteration
len = numel(stats_);

srs  = num2cell(repmat(pb.tcinfo.SR, [1,len]));  
rks  = num2cell(repmat(pb.params_pb.rank, [1,len]));  
ihps = num2cell(repmat(Ihp+tid, [1,len]));  


%% Let the "ite" id include the time info: starting time of the initial time (day-hr-min-sec) of the experiment (scripts/test*.m). 
ites = num2cell(repmat(Ite+tid, [1,len])); 
hps  = {num2str(pb.params_pb.paramd, '%1.1e,')};  

[stats_.SR] = srs{:};
[stats_.rank] = rks{:};
[stats_.Ihp] = ihps{:};
[stats_.Ite] = ites{:};
[stats_.parampb] = deal(hps{:});

end 
