function UtU = compute_UtU(U)

% Pre-compute UtU:
fds = fieldnames(U); 

for i = 1  : numel(fds)
	UtU{i} = (U.(fds{i}))'* U.(fds{i}); 
end

end
