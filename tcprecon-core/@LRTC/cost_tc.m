function [f, store] = cost_tc(U, store, tcinfo, params_pb)
% 
if ~isfield(store, 'val')
    if ~isfield(store,'PT')
        % store.PT = tspmaskmult(U.u1, U.u2, U.u3, tcinfo.I, tcinfo.J, tcinfo.K);
        store.PT = tspmaskmult(U.(tcinfo.unames{1}), U.(tcinfo.unames{2}), U.(tcinfo.unames{3}), tcinfo.I, tcinfo.J, tcinfo.K);
    end
    if ~isfield(store,'err_Omega')
        store.err_Omega = store.PT - tcinfo.Ttr;
    end
    % Possibly non-zero regularization by the Frobenius norms of the factor matrices
    regfun = 0; 
    if any(params_pb.paramd)
        for i = 1 : numel(tcinfo.unames)
            regfun = regfun + 0.5*params_pb.paramd(i)*norm(U.(tcinfo.unames{i}), 'fro')^2;
        end
    end
    % Evaluate the cost function 
    store.val = .5*sum(store.err_Omega.^2) + regfun; 
end
f = store.val;
end
