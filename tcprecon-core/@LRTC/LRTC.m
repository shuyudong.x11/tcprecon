classdef LRTC < Problem
    % Class of the CPD-based tensor completion problem.
    %
    % Reference:
    % Y. Guan, S. Dong, B. Gao, and P.-A. Absil and F. Glineur. Preconditioned
    % algorithms for CPD-based Tensor completion.
    %
    % Contact: Shuyu Dong (shuyu.dong@uclouvain.be), ICTEAM, UCLouvain.
    % Latest version: February, 2020.
    properties
        stepsz_estimator;
        statsfun;
        stopfun;
        
        Tstar;
        params_pb;
        tcinfo;
        tid; 
    end
    properties (Constant)
        default_pb = struct('paramd', zeros(1, 3),...
                            'delta_rgrad', 0, 'rank', 10);
        NAMES_MAN  = {'CPD_euc', 'CPD_precon', 'CPD_precon_mex', 'CPD_euc_mex'};
        KEYS_TCscores = {'RMSE','MSE','NRMSE','ND','MAPE',...
            'RMSE_tr','MSE_tr','NRMSE_tr','ND_tr','MAPE_tr',...
            'RMSE_t','MSE_t','NRMSE_t','ND_t','MAPE_t',...
            'relErr', 'SNR'};
        DEFAULT_rperi_names = {'niter', 'time', 'RMSE_t', 'RMSE', 'RMSE_tr', 'relErr', 'gradnorm', 'cost'};
    end
    methods (Static)
        function rowid = get_rowid_TCscores(key)
            rowid =  find(strcmp(key, LRTC.KEYS_TCscores));
        end
        function rowid = get_rowid_resPerIter(key)
            rowid =  find(strcmp(key, LRTC.DEFAULT_rperi_names));
        end
        function p = build_parser(paramset, p)
            % Build an input argument parser for a given struct of parameters.
            if nargin < 2
                % See https://nl.mathworks.com/help/matlab/ref/inputparser.html for
                % details about "inputParser".
                p = inputParser;
            end
            fds = fieldnames(paramset);
            for i = 1 : length(fds)
                if ~strcmp(fds{i},'Properties')
                    addParameter(p, fds{i}, paramset.(fds{i}));
                    % addOptional(p, fds{i}, paramset.(fds{i}));
                end
            end
            p.KeepUnmatched = true;
        end
        function SNR = computeSCORES_SNR(Xsol, Xclean)
            sn_ratio = sum(Xclean(:).^2) / sum((Xsol(:)-Xclean(:)).^2);
            SNR = 10 * log10(sn_ratio);
        end
        function TCscores = computeSCORES_TC(Delta, Ttar, rmse)
            % from 1 to 5: RMSE, MSE, NRMSE, ND, MAPE
            %
            % input: Delta,  Ttar must be of size [_ x 1].
            if nargin < 3
                MSE = mean(Delta.^2);
                rmse = sqrt(MSE);
            else
                MSE = rmse^2;
            end
            % mtemp2 = mean(abs(Ttar));
            
            TCscores = [rmse;...
                MSE;...
                nan;...
                nan;...
                nan ];
        end
        function mcscores = compute_NRMSEscores(mcscores, Ttar, Ttar_tr, Ttar_t)
            %
            rowid = [3; 8; 13];
            rowid_rmse = [1; 6; 11];
            mtemp2 = [mean(abs(Ttar(:))); mean(abs(Ttar_tr)); ...
                mean(abs(Ttar_t))];
            mcscores(rowid) = mcscores(rowid_rmse)./mtemp2;
        end
        % function tcinfo = genOMEGA_TC(Ttar, rho)
        % % Replaced by lrtc/build_tcinfo_frSpT.m
        % end
        function relErr = computeSCORES_relErr(mse, tcinfo)
            % Compute the relative error: |X-Xtar|_F / |Xtar|_F.
            distF = sqrt( (numel(tcinfo.I)+numel(tcinfo.Itest))*mse ) ;
            relErr = distF/sqrt(sum([tcinfo.Ttr;tcinfo.Tte].^2));
        end
        function stop  = stopfun_manopt(optpb, X, info, last)
            stop = false;
            if last>1 && info(last).gradnorm > 1e7 %           
                  stop = true;
            end
        end
        function stop  = stopfun_ml1m(optpb, X, info, last)
            stop = false;
            if last>1 && info(last).gradnorm > 1e7 %           
                  stop = true;
            end
            % stopcrit-622: Relative change in RMSE-tr drops below a certain tolerance after long enough time.
            % This criterion will be effective if the time budget criterion is not imposed (or is too large).
            if isfield(info(last), 'RMSE_tr') && isfield(optpb.params_opt, 'tol_relchg_rmse')
                if info(last).time > 10 && last > 10 
                    rmses = [info(last-6:last).RMSE_tr];
                    relchg = max( abs(diff(rmses))./rmses(1:end-1) );

                    % relchg = abs(info(last).RMSE_tr - info(last-1).RMSE_tr)/info(last).RMSE_tr;
                    if relchg < optpb.params_opt.tol_relchg_rmse
                        fprintf('....Stopping criterion ML1M triggered: Tol relchg-rmse reached\n'); 
                        stop = true;
                    end
                end
            end 
        end
        function val = compute_rperi_relErr(rmse_all, tcinfo)
            val = rmse_all /...
                sqrt( mean([tcinfo.Ttr;tcinfo.Tte].^2) ) ;
        end
        function val = compute_rperi_RMSE(Delta)
            val = sqrt(mean(Delta.^2));
        end
        function [stats] = statsfun_cv(optpb, U, stats, store, tcinfo)
            stats.RMSE_t  = NaN ;
            stats.RMSE    = NaN ;
            stats.RMSE_tr = NaN ;
            stats.relErr  = NaN ; 
        end
        function [stats] = statsfun_unified_(optpb, U, stats, store, tcinfo)
            if ~isfield(store,'PT')
                % store.PT = tspmaskmult(U{1}, U{2}, U{3}, tcinfo.I, tcinfo.J, tcinfo.K);
                store.PT = tspmaskmult(U.(tcinfo.unames{1}), U.(tcinfo.unames{2}), U.(tcinfo.unames{3}), tcinfo.I, tcinfo.J, tcinfo.K);
            end
            if ~isfield(store,'PT_t')
                % store.PT_t = tspmaskmult(U{1}, U{2}, U{3}, tcinfo.Ite, tcinfo.Jte, tcinfo.Kte);
                store.PT_t = tspmaskmult(U.(tcinfo.unames{1}), U.(tcinfo.unames{2}), U.(tcinfo.unames{3}), tcinfo.Ite, tcinfo.Jte, tcinfo.Kte);
            end
            if ~isfield(store,'err_Omega')
                store.err_Omega = store.PT - tcinfo.Ttr;
            end
            
            rmse_tr = LRTC.compute_rperi_RMSE(store.err_Omega);
            rmse_te = LRTC.compute_rperi_RMSE(store.PT_t-tcinfo.Tte);
            ntr     = tcinfo.sz_tr ;
            nte     = tcinfo.sz_te ;
            mse_all     = (rmse_tr^2 *ntr + rmse_te^2 *nte)/(ntr+nte) ;
            rmse_all    = sqrt( mse_all );
            relErr = LRTC.compute_rperi_relErr(rmse_all, tcinfo);
            
            stats.RMSE_t  = rmse_te ;
            stats.RMSE    = rmse_all ;
            stats.RMSE_tr = rmse_tr ;
            stats.relErr  = relErr ;
        end
        
        tcinfo        = build_tcinfo_frSpT(T, SR, seed)
        [f, store]    = cost_tc(U, store, tcinfo, params_pb)

        [rgrad,store] = rgrad_tc_precon(U, store, tcinfo, params_pb)
        [egrad,store] = egrad_tc(U, store, tcinfo, params_pb)        
        [rgrad,store] = rgrad_tc_precon_mex(U, store, tcinfo, params_pb)
        [egrad,store] = egrad_tc_mex(U, store, tcinfo, params_pb)
        
        [tmin, store] = stepsz_linemin(U, dir, store, tcinfo)
        yty           = compute_precon(U, tcinfo)
        utu           = compute_UtU(U)
        X             = initialize_cpd(tcinfo, rank, method_init)
    
        function [man, costfun, gradfun, hessfun, precon, lsfun] = construct_pb(manifoldname, tcinfo, params_pb)
            switch manifoldname
                case 'external'
                    man = struct(); 
                    costfun = @(U, store) NaN; 
                    gradfun = @(U, store) NaN; 
                    lsfun   = @(U, dir, store) NaN; 
                case 'CPD_precon_mex'
                    prodsp = struct();
                    for i = 1 : numel(tcinfo.sz_tensor) %ndims(Xtar)
                        prodsp.(tcinfo.unames{i}) =  euclideanfactory(tcinfo.sz_tensor(i), params_pb.rank);
                    end
                    man      = productmanifold(prodsp);
                    man.name = @() manifoldname;
                    costfun  = @(U, store)LRTC.cost_tc(U, store, tcinfo, params_pb);
                    %%%% attention: one more input argument.
                    gradfun  = @(U, store) LRTC.rgrad_tc_precon_mex(U, store, tcinfo, params_pb);
                    lsfun    = @(U, dir, store) LRTC.stepsz_linemin(U, dir, store, tcinfo);
                case 'CPD_euc_mex'
                    prodsp = struct();
                    for i = 1 : numel(tcinfo.sz_tensor) %ndims(Xtar)
                        prodsp.(tcinfo.unames{i}) =  euclideanfactory(tcinfo.sz_tensor(i), params_pb.rank);
                    end
                    man      = productmanifold(prodsp);
                    man.name = @() manifoldname;
                    costfun  = @(U, store)LRTC.cost_tc(U, store, tcinfo, params_pb);
                    gradfun  = @(U, store) LRTC.egrad_tc_mex(U, store, tcinfo, params_pb);
                    lsfun    = @(U, dir, store) LRTC.stepsz_linemin(U, dir, store, tcinfo);                    
                case 'CPD_precon'
                    prodsp = struct();
                    for i = 1 : numel(tcinfo.sz_tensor) %ndims(Xtar)
                        prodsp.(tcinfo.unames{i}) =  euclideanfactory(tcinfo.sz_tensor(i), params_pb.rank);
                    end
                    man      = productmanifold(prodsp);
                    man.name = @() manifoldname;
                    costfun  = @(U, store)LRTC.cost_tc(U, store, tcinfo, params_pb);
                    %%%% attention: one more input argument.
                    gradfun  = @(U, store) LRTC.rgrad_tc_precon(U, store, tcinfo, params_pb);
                    lsfun    = @(U, dir, store) LRTC.stepsz_linemin(U, dir, store, tcinfo);
                case 'CPD_euc'
                    prodsp = struct();
                    for i = 1 : numel(tcinfo.sz_tensor) %ndims(Xtar)
                        prodsp.(tcinfo.unames{i}) =  euclideanfactory(tcinfo.sz_tensor(i), params_pb.rank);
                    end
                    man      = productmanifold(prodsp);
                    man.name = @() manifoldname;
                    costfun  = @(U, store)LRTC.cost_tc(U, store, tcinfo, params_pb);
                    gradfun  = @(U, store) LRTC.egrad_tc(U, store, tcinfo, params_pb);
                    lsfun    = @(U, dir, store) LRTC.stepsz_linemin(U, dir, store, tcinfo);
                otherwise
                    error(sprintf('%s is not implemented yet in Manopt..\n', manifoldname) );
            end
            if ~exist('hessfun', 'var')
                hessfun = [];
            end
            if ~exist('precon', 'var')
                precon = [];
            end
            man.name    = @() manifoldname;
        end
    end % (methods (Static))
    
    methods
        function self = LRTC(manifoldname, Xtar, tcinfo, params_pb)
            if nargin < 1, manifoldname = LRTC.NAMES_MAN{1}; end
            % if ~any(strcmp(manifoldname, LRTC.NAMES_MAN)), manifoldname = LRTC.NAMES_MAN{1}; end
            if nargin < 2
                % TODO: complete gendata_LR.m
                Xtar = LRTC.gendata_LR(300,300,300, 10);
            end
            sz_tensor = size(Xtar);
            if nargin < 3
                % TODO: complete gendata_LR.m
                tcinfo = LRTC.build_tcinfo_frSpT(Xtar, 0.2);
            end
            if nargin < 4, params_pb = LRTC.default_pb; end
            
            %% Construct function handles.
            [man, costfun, gradfun, hessfun, precon, lsfun] = LRTC.construct_pb(manifoldname, tcinfo, params_pb);
            
            %% Initialize class properties.
            self = self@Problem(man, costfun, gradfun,[],hessfun, []);
            
            self.stepsz_estimator = lsfun;
            self.stopfun = @(optpb, X, info, last)LRTC.stopfun_manopt(optpb, X, info, last);
            self.statsfun = @(optpb, X, stats, store)LRTC.statsfun_unified_(optpb, X, stats, store, tcinfo);
            self.Tstar      = Xtar;
            self.params_pb  = params_pb;
            self.tcinfo     = tcinfo;
        end
        function set_samplrate(self, SR)
            % This function refreshes the problem class by generating a new
            % set of revealed entries at the given sampling rate SR.
            if nargin < 1
                SR = 0.2;
            end
            tcinfo = LRTC.build_tcinfo_frSpT(self.Tstar, SR);
            self.refresh_obj([], tcinfo);
        end
        function loadnew_paramspb(self, params_pb)
            % Set up parameter values by a given struct of parameters.
            self.refresh_obj([], [], params_pb);
        end
        function set_params_pb(self, varargin)
            % Set up parameter values by the pair of keyword and value. The parameters are
            % 1. paramd.
            % 2. rank. Note that the parameter sampl_rate is related to the given data.
            % Warning: the parser only admits keywords prescribed in LRTC.default_pb.
            params_pb = self.params_pb;
            parser = LRTC.build_parser(params_pb);
            parse(parser, varargin{:});
            fds = fieldnames(parser.Results);
            for i = 1 : length(fds)
                params_pb.(fds{i}) = parser.Results.(fds{i});
            end
            self.refresh_obj([], [], params_pb);
        end
        function refresh_obj(self, manifold_name, tcinfo, params_pb)
            if nargin < 2
                manifold_name = [];
            end
            if nargin < 3
                tcinfo = [];
            end
            if nargin < 4
                params_pb = [];
            end
            if isempty(manifold_name)
                manifold_name = self.manifold.name();
            end
            if isempty(tcinfo)
                tcinfo = self.tcinfo;
            end
            if isempty(params_pb)
                params_pb = self.params_pb;
            end
            %% Reconstruct function handles.
            [man, costfun, gradfun, hessfun, precon, lsfun] = LRTC.construct_pb(manifold_name, tcinfo, params_pb);
            
            %% Reload class properties.
            self.refresh_obj@Problem(man, costfun, gradfun,[],hessfun, []);
            self.stepsz_estimator = lsfun;
            self.params_pb = params_pb;
            self.tcinfo = tcinfo;
        end
    end
end

