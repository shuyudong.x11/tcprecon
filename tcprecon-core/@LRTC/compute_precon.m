function [YiTYi, UtU] = compute_precon(U, tcinfo)
% INPUT: UtU = compute_UtU(U); 
% OUTPUT: YiTYi{i}, for i = 1,...,k.

% Compute the preconditioning operator.

UtU = LRTC.compute_UtU(U); 
k = numel(tcinfo.unames);
for i = 1:k
		YiTYi{i} = UtU{tcinfo.ind_neqi{i}(1)};
		% Hadamard product: 
		for j = 2 : k-1
        YiTYi{i} = YiTYi{i} .* UtU{tcinfo.ind_neqi{i}(j)} ;
        end
end
