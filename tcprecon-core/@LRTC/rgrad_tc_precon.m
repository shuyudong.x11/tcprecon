function [rgrad, store] = rgrad_tc_precon(U, store, tcinfo, params)
    if ~isfield(store, 'egrad')
        if ~isfield(store,'PT')
            % store.PT = tspmaskmult(U{1}, U{2}, U{3}, tcinfo.I, tcinfo.J, tcinfo.K);
            store.PT = tspmaskmult(U.(tcinfo.unames{1}), U.(tcinfo.unames{2}), U.(tcinfo.unames{3}), tcinfo.I, tcinfo.J, tcinfo.K);
        end
        if ~isfield(store,'err_Omega')
            store.err_Omega = store.PT - tcinfo.Ttr;
        end
        if ~isfield(store, 'KRU_neqi')
            for i = 1 : 3 % the line below applies only to k=3 (and not generalized yet to higher-order tensors)
                store.KRU_neqi{i} = khatrirao(U.(tcinfo.unames{tcinfo.ind_neqi{i}(2)}), ...
                                              U.(tcinfo.unames{tcinfo.ind_neqi{i}(1)}));
            end
        end
        for i = 1 : numel(tcinfo.unames)
            S{i} = sparse(tcinfo.Omega_mat{i}(:,2), tcinfo.Omega_mat{i}(:,1), ...
                          store.err_Omega, tcinfo.sz_tensor(i), tcinfo.sz_prodneqi(i));
            % egrad{i} = S{i} * store.KRU_neqi{i} ; 
            gradreg = params.paramd(i) * U.(tcinfo.unames{i}); 
            egrad.(tcinfo.unames{i}) = S{i} * store.KRU_neqi{i} + gradreg; 
        end
        store.egrad = egrad ;
    else
        egrad = store.egrad ;
    end
    % Compute preconditioner.
    YiTYi = LRTC.compute_precon(U, tcinfo);
    for i = 1 : numel(tcinfo.unames)
        % rgrad{i} = egrad{i} / (YiTYi{i} + params.delta_rgrad*eye(params.rank)); 
        rgrad.(tcinfo.unames{i}) = egrad.(tcinfo.unames{i}) / (YiTYi{i} + params.delta_rgrad*eye(params.rank)); 
    end 
end

