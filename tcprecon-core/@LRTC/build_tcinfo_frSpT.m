function tcinfo = build_tcinfo_frSpT(T, SR, seed)
% This function creates a standard format for the input entries  which is composed of
% - I, J, K, Ttr:           Input entries of the training set in COO format 
% - Ite, Jte, Kte, Tte:     Input entries of the test set in COO format 
% - unames:                 Field names of the CPD factor matrices of a tensor in CPD form. 
% 

if nargin < 2
    SR = .8; 
end
if nargin < 3
    use_seed = false;
else
    use_seed = true;
end

[c1,c2] = find(T);
dims = size(T); 
nz = size(c1,1);
ntotal = prod(dims); 

if nz<ntotal%issparse(T)
    % Extract training data among entries that are available in T 
    % [c1,c2] = find(T);
    N = ceil(SR*nz);
    if use_seed 
        rng(seed);
    end
    s_tr = randsample(nz, N);
    s_tr = sort(s_tr);
    ntr  = numel(s_tr); 

    s_comp = setdiff([1:nz], s_tr);
    ncomp  = numel(s_comp); 

    s_te_comp = s_comp(randsample(ncomp, min(ncomp,ntr)));
    s_te = s_comp(randsample(ncomp, ceil(min(ncomp,ntr)/8)));

    % s_te = setdiff(1:nz, s_tr);
    % nte  = numel(s_te); 
    % if nte > 2e6
    %     % in case the complementary set is too large for large dense data tensors, just subsample a fraction from it
    %     % s_te = setdiff(1:ceil(numel(s_tr)/4), s_tr);
    %     % s_te_comp = setdiff(1:ceil(numel(s_tr)), s_tr);
    %     s_te_comp = s_te;
    %     s_te = s_te(randsample(nte, ceil(0.1*nte)));
    % end
    if size(s_te,2) > 1 
        s_te = s_te';
    end
    if size(s_te_comp,2) > 1 
        s_te_comp = s_te_comp';
    end
    
    I = c1(s_tr,1);
    J = c1(s_tr,2);
    K = c1(s_tr,3);
    
    Ite = c1(s_te,1);
    Jte = c1(s_te,2);
    Kte = c1(s_te,3);

    Ite_comp = c1(s_te_comp,1);
    Jte_comp = c1(s_te_comp,2);
    Kte_comp = c1(s_te_comp,3);
    
    Ttr = c2(s_tr); 
    Tte = c2(s_te); 
    Tte_comp = c2(s_te_comp); 
else
    % Extract training data among all entries of T
    nz = prod(dims); 
    N = ceil(SR*nz);
    if use_seed 
        rng(seed);
    end
    s_tr = randsample(nz, N);
    s_tr = sort(s_tr);
    ntr  = numel(s_tr); 

    s_comp = setdiff([1:nz], s_tr);
    ncomp  = numel(s_comp); 

    s_te_comp = s_comp; %s_comp(randsample(ncomp, min(ncomp,ntr)));
    s_te = s_comp(randsample(ncomp, ceil(min(ncomp,ntr)/8)));

    % if nte > 2e6
    %     % In case the complementary set is too large for large dense data tensors, just subsample a fraction from it
    %     % s_te = setdiff(1:ceil(numel(s_tr)/4), s_tr);
    %     % s_te_comp = setdiff(1:ceil(numel(s_tr)), s_tr);
    %     s_te_comp = s_te(randsample(nte, min(ntr,nte)));
    %     s_te = s_te(randsample(nte, ceil(min(ntr,nte)/8)));
    % end
    % Possible compatiblility issue with MATLAB: make sure s_te is a n-by-1
    % array 
    if size(s_te,2) > 1 
        s_te = s_te';
    end
    if size(s_te_comp,2) > 1 
        s_te_comp = s_te_comp';
    end
    
    [I,J,K] = ind2sub(dims, s_tr);
    [Ite,Jte,Kte] = ind2sub(dims, s_te);
    [Ite_comp,Jte_comp,Kte_comp] = ind2sub(dims, s_te_comp);
    % The tensor T must be given in the 'tensor' format within the tensor-toolbox 
    Ttr = T.data(s_tr); 
    Tte = T.data(s_te); 
    Tte_comp = T.data(s_te_comp); 
end

%% 
trans = [];
Omega = [I,J,K];
for i = 1:3
    bte{i} = setdiff(1:3,i);
    sz_prodneqi(i) = prod(dims(bte{i}));
    Omega_temp = Omega(:,[bte{i},i]);
   for j = 1:N
       trans(j,i) = Omega_temp(j,1) + (Omega_temp(j,2)-1)*dims(bte{i}(1));
   end
end
Omega_mat{1} = [trans(:,1),I]; 
Omega_mat{2} = [trans(:,2),J];
Omega_mat{3} = [trans(:,3),K]; 

unames = {'u1','u2','u3'};


%% 

tcinfo = struct('I', uint32(I), 'J', uint32(J), 'K', uint32(K),...
                'Ite', uint32(Ite), 'Jte', uint32(Jte), 'Kte', uint32(Kte),...    
                'Ite_comp', uint32(Ite_comp), 'Jte_comp', uint32(Jte_comp), 'Kte_comp', uint32(Kte_comp),...    
                'Ttr', Ttr, 'Tte', Tte,...
                'Tte_comp', Tte_comp,...
                'SR',  SR, 'sz_tr',numel(I), 'sz_te',numel(Ite),...
                'sz_te_comp', numel(Ite_comp), ... 
                'sz_tensor', dims, 'sz_prodneqi', sz_prodneqi, ... 
                'ind_neqi', {bte}, ...
                'unames', {unames}, ...
                'Omega_mat', {Omega_mat}); 

end
