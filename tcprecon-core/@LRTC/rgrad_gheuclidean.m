function [grad, store] = rgrad_gheuclidean(X, store, mcinfo)
    if ~isfield(store, 'grad')
        if ~isfield(store,'PX')
            store.PX = spmaskmult(X.G,(X.H'), mcinfo.I, mcinfo.J);
        end
        if ~isfield(store,'err_Omega')
            store.err_Omega = store.PX - mcinfo.PXtar;
        end
        S = sparse(double(mcinfo.I), double(mcinfo.J), store.err_Omega,...
                    mcinfo.size_M(1), mcinfo.size_M(2));

        store.grad = struct('G', S*X.H,...
                            'H', S'*X.G );
    end
    grad = store.grad ;   
end

