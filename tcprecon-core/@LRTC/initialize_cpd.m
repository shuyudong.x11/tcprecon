function U = initialize_cpd(tcinfo, rank, method_init)

m = tcinfo.sz_tensor; 
k = numel(m);

if nargin < 3
  method_init = 'random';
end

switch method_init
case 'HOSVD'
  Z = sptensor([double(tcinfo.I), double(tcinfo.J), ...
                double(tcinfo.K)], tcinfo.Ttr, m);  
  Z1 = tenmat(Z,1);
  Z2 = tenmat(Z,2);
  Z3 = tenmat(Z,3);
  U = {};
  [U{1},~,~] = svds(Z1.data,rank);
  [U{2},~,~] = svds(Z2.data,rank);
  [U{3},~,~] = svds(Z3.data,rank);
  lambda = [];
  for j = 1: rank 
      temp{1} = U{1}(:,j);
      temp{2} = U{2}(:,j);
      temp{3} = U{3}(:,j);
      temp_tensor = ktensor(temp);
      % Rank-1 component [U^{(1)}_{j}, U^{(2)}_{j}, U^{(3)}_{j}]. 
      lambda(j) = innerprod(Z, temp_tensor);
      if lambda(j) < 0
          lambda(j) = -lambda(j);
          U{1}(:,j) = -U{1}(:,j);
      end
  end
  for i = 1:k
      for j = 1 : rank 
          U{i}(:,j) = lambda(j).^(1/3)*U{i}(:,j);
      end
  end        
case 'random'
    U = {};
    for i = 1:k
       U{i} = randn(m(i), rank); % randomly generate each factor
    end
end

end
