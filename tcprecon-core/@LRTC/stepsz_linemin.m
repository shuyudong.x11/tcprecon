function [tmin, store, ts, dfts] = stepsz_linemin(x, dirr, store, tcinfo)
% compute the initial step-size
% Exact line search in the direction of dir on the tangent space of x
% !! so NOT the retracted curve, only use as guess !!
% x, current point
% dir is search direction
%
% Returns: tmin

% The 5rd-order polynomial is
%   P(t)=c_1 + 2*c_2 *t + 3*c_3 t^2 + 4*c_4 t^3 + 6*c_6 t^5.
%   the roots of P (in matlab) is: roots([1:4].*c), where c = [c_1,..,c_4,c_6].
%   the exact line min. objective is O(t)= sum_{j=1}^6 c_j t^j.
for i = 1  : numel(tcinfo.unames)
  U{i} = x.(tcinfo.unames{i});
  dir{i} = dirr.(tcinfo.unames{i});
end


   if ~isfield(store,'PT')
       store.PT = tspmaskmult(U{1}, U{2}, U{3}, tcinfo.I, tcinfo.J, tcinfo.K);
   end
   if ~isfield(store,'err_Omega')
       store.err_Omega = store.PT - tcinfo.Ttr;
   end
   
   % dir.R = 
   if ~isfield(store,'pomega_teta_1')
       store.pomega_teta_1 = tspmaskmult( dir{1}, U{2}, U{3}, tcinfo.I, tcinfo.J, tcinfo.K)+...
                           tspmaskmult( U{1}, dir{2}, U{3}, tcinfo.I, tcinfo.J, tcinfo.K)+...
                           tspmaskmult( U{1}, U{2}, dir{3}, tcinfo.I, tcinfo.J, tcinfo.K);

   end
   if ~isfield(store,'pomega_teta_2')
       store.pomega_teta_2 = tspmaskmult( dir{1}, dir{2}, U{3}, tcinfo.I, tcinfo.J, tcinfo.K)+...
                           tspmaskmult( dir{1}, U{2}, dir{3}, tcinfo.I, tcinfo.J, tcinfo.K)+...
                           tspmaskmult( U{1}, dir{2}, dir{3}, tcinfo.I, tcinfo.J, tcinfo.K);

   end
   if ~isfield(store,'pomega_eta')
       store.pomega_eta = tspmaskmult(dir{1}, dir{2}, dir{3}, tcinfo.I, tcinfo.J, tcinfo.K);
   end

   c1 = (store.err_Omega') * store.pomega_teta_1;

   c2 = 0.5*sum(store.pomega_teta_1.^2) + (store.err_Omega')*store.pomega_teta_2;
   
   c3 = (store.err_Omega') * store.pomega_eta + (store.pomega_teta_1')*store.pomega_teta_2;

   c4 = 0.5*sum(store.pomega_teta_2.^2) + (store.pomega_teta_1')*store.pomega_eta;
   
   c5 = (store.pomega_teta_2')*store.pomega_eta;
   
   c6 = 0.5*sum(store.pomega_eta.^2) ;
   
   
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
   c = [c1,c2,c3,c4,c5,c6];
   ts = roots(fliplr([1:6].*c));

   ts = ts(imag(ts)==0);
   ts = ts(ts>0);
   if isempty(ts)
       ts = 1;
   end

   dfts = polyval([c6 c5 c4 c3 c2 c1 0], ts);
   [~, iarg] = min(dfts);
   tmin = ts(iarg);
end

