function test_rgeqstar_oursBBs(SR, SNR, sid)
% Test the various types of BB stepsize rules for our RGD algorithm. 
% Last update: May 2020.

%% Information for generating the synthetic dataset
GDATTYPE_G       = {'community', 'sensor', 'random_k', 'erdos-renyi'} ; 
GDATTYPE_Z       = {'graph-fullrank', 'graph-lowrank', 'graph-agnostic'} ;
GDATTYPE_SPECFUN = {'tikhonov', 'pseudoinv', 'diffusion'};

GDATSIZES        = [100, 100, 100];
GDATRANK         = repmat(10, [1,3]); % Tucker rank  
GDATTYPE_gr      = GDATTYPE_G{1};
GDATTYPE_z       = GDATTYPE_Z{2};
GDATTYPE_spec    = GDATTYPE_SPECFUN{2};
GDATPARAM_spec   = 1; 
GDAT_TARGETKAPPA = nan;
GDAT_SCALE       = 1; 
GDATNOISE        = [1, 40]; % the first number being 0 means it is "noiseless". 

SR_MIN = 0.1; % 0.20;
SR_MAX = 0.1; %0.20;
SR_N   = 1;

%% Generate or load data 
% load datasets/syn_data_100_without.mat
data = gen_gtensor('gdatSizes',         GDATSIZES,... 
                'gdatRank',             GDATRANK,...
                'gdatType_Z',           GDATTYPE_z,...
                'gdatType_g',           GDATTYPE_gr,...
                'gdatParam_g',          nan,...
                'gdatType_specfun',     GDATTYPE_spec,...
                'gdatType_specfun_doTruncate',  false,...
                'gdatParam_specfun',    GDATPARAM_spec,...
                'gdatParam_targetKappa',GDAT_TARGETKAPPA,...
                'gdatParam_scale',      GDAT_SCALE,...
                'gdatNoise',            GDATNOISE);
%% Set up the range of sampling rates
[SRs] = linspace(SR_MIN, SR_MAX, SR_N);

%% Generate problem parameters (Reg, CPD rank, Qprecon-delta)
IV_PARAMD = [0 0 0; 0 0 0];
IV_RANK   = floor([1.0; 1.5]*max(GDATRANK));
NHPs      = 2; 
[arr_hps, tab_hps] = gen_hps('n_hps', NHPs, 'pb_paramd', IV_PARAMD, ...
                             'pb_rank', IV_RANK); 
arr_hps = arr_hps(end); 
tab_hps = tab_hps(end,:); 
fprintf('The parameter settings to test are:\n');
disp(tab_hps);

%% Generate the list of methods to test
% Methods available for test are: 
% 1  Qprecon RGD (linemin)
% 2  Qprecon RGD (Armijo)
% 3  Qprecon RCG (linemin)
% 4  Qprecon RCG (Armijo)
% 5  Qprecon GD (BB)
% 6  Euclidean GD (linemin)
% 7  Euclidean GD (Armijo)
% 8  Euclidean CG (linemin)
% 9  Euclidean CG (Armijo)
% 10  Euclidean GD (BB)
% 11  KM16 (eqrks) % "eqrks" means it uses (r1,r2,r3) with r1=r2=r3=our-PCD-rank, the default solver is Manopt's RCG. 
% 12  AltMin
% 13  INDAFAC
% 14  CP-WOPT
% 15  FaLRTC
% 16  SiLRTC
% 17  HaLRTC
% 18  KM16 (rstar) % "rstar" means the Tucker rank is set by given values, eg. the tucker rank of the true hidden tensor if it is known a priori.
% 19  TNCP
% 20  airCP
% 21  Qprecon RGD BB1
% 22  Qprecon RGD BB2
% 23  Qprecon RGD rBB
% 24  Qprecon RGD rBB1
% 25  Qprecon RGD rBB2

% tid = {1, 2, 3, 4, 5, 8, 9, 11, 18}; 
tid = {1, 3, 5, 21, 22, 23, 24, 25}; 
VALS_DELTA = [0]; %[0, 1e-3]; %[0, 1e-6, 1e-4, 1e-2]; 
% [minfos, mlabels] = gen_minfos('opt_delta_rgrad', vals, <other-opt-options>);  
[minfos, tab_m, mlabels] = gen_minfos(tid, 'precon_delta_rgrad', VALS_DELTA);  
fprintf('The methods to test are:\n');
disp(mlabels);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%% Set up optimization-related parameters
method_init = 'random'; %'HOSVD';  %size=[100,100,100]
MAXTIME = 10;
MAXIT = 200;
TOL = 1e-7;
Nte = 1; 
VERBO = 2; 

res_rperi = {}; 
irun = 0; 
for i = 1 : numel(SRs)
    for tt = 1 : Nte
        tcinfo = LRTC.build_tcinfo_frSpT(data.tensor, SRs(i));

        for j = 1 : numel(arr_hps)
            t0 = tic;
            Xinit = LRTC.initialize_cpd(tcinfo, arr_hps(j).rank, method_init);  %initial_U_testtensor(tcinfo, opts);
            tinit = toc(t0);
            for ii = 1 : numel(minfos)
                switch minfos(ii).manifold 
                    case {'Qprecon', 'KM16'} 
                        pb = LRTC('CPD_precon', data.tensor, tcinfo);
                    case 'Embedded'
                        % pb = LRTC('CPD_embedded', data.tensor, tcinfo); % 
                        error('The manifold structure embedded is not available'); 
                    case 'Euclidean'
                        pb = LRTC('CPD_euc', data.tensor, tcinfo);
                    otherwise 
                        error('The method %s is not available yet..\n', minfos(ii).name); 
                end
                % Set up the CPD rank. The methods concerned are those implmemented in
                % @Solver. 
                pb.set_params_pb('rank', arr_hps(j).rank); 
                pb.set_params_pb('paramd', arr_hps(j).paramd);

                % Note: The optional input argument 'km16_tuckerrank' is only used by the 
                % method "KM16 (rstar)". In case it is to be tested, the value of this input 
                % is passed to the dimensions of the core tensor of KM16's problem object.  
                [X_, stats_, runinfo] = runmethods_fromlist(pb, Xinit, minfos(ii), ...
                                                   'maxtime', MAXTIME, 'maxiter', MAXIT,...
                                                   'tolgradnorm', TOL,...
                                                   'verbosity', VERBO,...
                                                   'km16_tuckerrank', data.tuckerrank); 
                irun = irun + 1; 
                %% Collect results in a container 
                % res_rperi{i, ii, tt} = get_rperi_unified(stats_); 
                for t = 1 : numel(stats_)
                    stats_(t).time = stats_(t).time + tinit; 
                end
                res_rperi{i, j, ii, tt} = stats_;
                testinfo = [struct2table(struct('rid', irun, 'SR', SRs(i),...
                                                'iHP', j, 'iTe', tt)), runinfo]; 
                runinfos(irun) = table2struct(testinfo);  
                fprintf('....Method: %s finished. The test info was: \n',...
                        minfos(ii).name); 
                disp(testinfo); 
            end
        end
   end
end

%% Produce figures 
fprintf('Producing figures...\n ');
% to update 
for j = 1 : numel(arr_hps)
    cura = produce_rperi_fromRes(res_rperi, minfos, SRs, 1, j, 'time', 'RMSE_t');
    cura2 = produce_rperi_fromRes(res_rperi, minfos, SRs, 1, j, 'time', 'RMSE_tr');
    curb = produce_rperi_fromRes(res_rperi, minfos, SRs, 1, j, 'time', 'gradnorm');
    curc = produce_rperi_fromRes(res_rperi, minfos, SRs, 1, j, 'iter', 'RMSE_t');
    curc2 = produce_rperi_fromRes(res_rperi, minfos, SRs, 1, j, 'iter', 'RMSE_tr');
    % curx = produce_rperi_fromRes(res_rperi, minfos, SRs, 1, j, 'iter', 'time');

    ha(j) = produce_fig_rperi(cura);
    ha2(j) = produce_fig_rperi(cura2);
    hb(j) = produce_fig_rperi(curb);
    hc(j) = produce_fig_rperi(curc);
    hc2(j) = produce_fig_rperi(curc2);
    % hx(j) = produce_fig_rperi(curx);
end

%% Save figures 
tmps= datetickstr(now,'HHMM_ddmmm');
timeStr = tmps{1};
% fdir = sprintf('./figs/%s/', pb.tid); 
fdir = sprintf('./figs/%s/', timeStr); 

save_figs(ha,'fig', sid, fdir); save_figs(ha,'png', sid, fdir);
save_figs(ha2,'fig', sid, fdir); save_figs(ha2,'png', sid, fdir);
save_figs(hb,'fig', sid, fdir); save_figs(hb,'png', sid, fdir);
save_figs(hc,'fig', sid, fdir); save_figs(hc,'png', sid, fdir);
save_figs(hc2,'fig', sid, fdir); save_figs(hc2,'png', sid, fdir);

% save_figs(hx,'fig', '', fdir); save_figs(hx,'png', '', fdir);

%% Record runinfo of all tests 
fprintf('writting runinfos into log file...\n ');
writetable(struct2table(runinfos), sprintf('%s/runinfos.csv', fdir));


