clear; close all;

%% Load data: movie lens 100k
dataname = 'ML1M'; 
data = load_tensor(dataname); 

%% Set up the range of sampling rates
SR_MIN = 0.8; % 0.20;
SR_MAX = 0.5; %0.20;
SR_N   = 1;
[SRs] = linspace(SR_MIN, SR_MAX, SR_N);

%% Generate problem parameters (Reg, CPD rank, Qprecon-delta)
RANK0 = 10;
IV_PARAMD = [0 0 0; 1 1 1];
IV_RANK   = floor([1.0; 1.0]*max(RANK0));
NHPs      = 4; 
[arr_hps, tab_hps] = gen_hps_2('do_randomsearch', [0, 0],...
                               'n_hps', NHPs, 'pb_paramd', IV_PARAMD, ...
                               'pb_rank', IV_RANK); 
arr_hps = arr_hps(end); 
tab_hps = tab_hps(end,:); 
fprintf('The parameter settings to test are:\n');
disp(tab_hps);
pause(3); 
% return;

%% Generate the list of methods to test
% Methods available for test are: 
% 1  Qprecon RGD (linemin)
% 2  Qprecon RGD (Armijo)
% 3  Qprecon RCG (linemin)
% 4  Qprecon RCG (Armijo)
% 5  Qprecon GD (BB)
% 6  Euclidean GD (linemin)
% 7  Euclidean GD (Armijo)
% 8  Euclidean CG (linemin)
% 9  Euclidean CG (Armijo)
% 10  Euclidean GD (BB)
% 11  KM16 (eqrks) % "eqrks" means it uses (r1,r2,r3) with r1=r2=r3=our-PCD-rank, the default solver is Manopt's RCG. 
% 12  AltMin
% 13  INDAFAC
% 14  CP-WOPT
% 15  FaLRTC
% 16  SiLRTC
% 17  HaLRTC
% 18  KM16 (rstar) % "rstar" means the Tucker rank is set by given values, eg. the tucker rank of the true hidden tensor if it is known a priori.
% 19  TNCP
% 20  airCP
% 21  Qprecon RGD BB1
% 22  Qprecon RGD BB2
% 23  Qprecon RGD rBB
% 24  Qprecon RGD rBB1
% 25  Qprecon RGD rBB2
% 26  Qprecon_mex RGD linemin
% 27  Euclidean_mex RGD linemin

mid = {'Qprecon RGD BB1', 'Qprecon RCG linemin',...
       'Qprecon_mex RGD BB1', 'Qprecon_mex RCG linemin'}; 
VALS_DELTA = [0]; %[0, 1e-3]; %[0, 1e-6, 1e-4, 1e-2]; 
% [minfos, mlabels] = gen_minfos('opt_delta_rgrad', vals, <other-opt-options>);  
[minfos, tab_m, mlabels] = gen_minfos(mid, 'precon_delta_rgrad', VALS_DELTA);  
fprintf('The methods to test are:\n');
disp(mlabels);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%% Set up optimization-related parameters
method_init = 'random'; %'HOSVD';  %size=[100,100,100]
MAXTIME = 100;
MAXIT = 2000;
TOL = 1e-7;
Nte = 1; 
VERBO = 2; 

res_rperi = {}; 
irun = 0; 
for i = 1 : numel(SRs)
    for tt = 1 : Nte
        tcinfo = LRTC.build_tcinfo_frSpT(data.tensor, SRs(i), tt);
        for j = 1 : numel(arr_hps)
            t0 = tic;
            Xinit = LRTC.initialize_cpd(tcinfo, arr_hps(j).rank, method_init);  %initial_U_testtensor(tcinfo, opts);
            tinit = toc(t0);
            for ii = 1 : numel(minfos)
                switch minfos(ii).manifold 
                    case {'Qprecon', 'KM16'} 
                        pb = LRTC('CPD_precon', data.tensor, tcinfo);
                    case {'Qprecon_mex'} 
                        pb = LRTC('CPD_precon_mex', data.tensor, tcinfo);
                    case 'Embedded'
                        % pb = LRTC('CPD_embedded', data.tensor, tcinfo); % 
                        error('The manifold structure embedded is not available'); 
                    case 'Euclidean'
                        pb = LRTC('CPD_euc', data.tensor, tcinfo);
                    case 'Euclidean_mex'
                        pb = LRTC('CPD_euc_mex', data.tensor, tcinfo);                    otherwise 
                        error('The method %s is not available yet..\n', minfos(ii).name); 
                end
                % Set up the CPD rank. The methods concerned are those implmemented in
                % @Solver. 
                pb.set_params_pb('rank', arr_hps(j).rank); 
                pb.set_params_pb('paramd', arr_hps(j).paramd);
                % Note: The optional input argument 'km16_tuckerrank' is only used by the 
                % method "KM16 (rstar)". In case it is to be tested, the value of this input 
                % is passed to the dimensions of the core tensor of KM16's problem object.  
                [X_, stats_, runinfo] = runmethods_fromlist(pb, Xinit, minfos(ii), ...
                                                   'maxtime', MAXTIME, 'maxiter', MAXIT,...
                                                   'tolgradnorm', TOL,...
                                                   'verbosity', VERBO); 
                irun = irun + 1; 
                %% Collect results in a container 
                % res_rperi{i, ii, tt} = get_rperi_unified(stats_); 
                for t = 1 : numel(stats_)
                    stats_(t).time = stats_(t).time + tinit; 
                end
                res_rperi{i, j, ii, tt} = stats_;
                testinfo = [struct2table(struct('rid', irun, 'SR', SRs(i),...
                                                'iHP', j, 'iTe', tt)), runinfo]; 
                runinfos(irun) = table2struct(testinfo);  
                fprintf('....Method: %s finished. The test info was: \n',...
                        minfos(ii).name); 
                disp(testinfo);
                pause(2); 
            end
        end
   end
end

%% Produce figures 
fprintf('Producing figures...\n ');
% to update 
for j = 1 : numel(arr_hps)
    cura = produce_rperi_fromRes(res_rperi, minfos, SRs, 1, j, 'time', 'RMSE_t');
    cura2 = produce_rperi_fromRes(res_rperi, minfos, SRs, 1, j, 'time', 'RMSE_tr');
    curb = produce_rperi_fromRes(res_rperi, minfos, SRs, 1, j, 'time', 'gradnorm');
    curc = produce_rperi_fromRes(res_rperi, minfos, SRs, 1, j, 'iter', 'RMSE_t');
    curc2 = produce_rperi_fromRes(res_rperi, minfos, SRs, 1, j, 'iter', 'RMSE_tr');
    % curx = produce_rperi_fromRes(res_rperi, minfos, SRs, 1, j, 'iter', 'time');

    ha(j) = produce_fig_rperi(cura);
    ha2(j) = produce_fig_rperi(cura2);
    hb(j) = produce_fig_rperi(curb);
    hc(j) = produce_fig_rperi(curc);
    hc2(j) = produce_fig_rperi(curc2);
    % hx(j) = produce_fig_rperi(curx);
end

%% Save figures 
tmps= datetickstr(now,'HHMM_ddmmm');
timeStr = tmps{1};
% fdir = sprintf('./figs/%s/', pb.tid); 
fdir = sprintf('./figs/%s/', timeStr); 

save_figs(ha,'fig', '', fdir); save_figs(ha,'png', '', fdir);
save_figs(ha2,'fig', '', fdir); save_figs(ha2,'png', '', fdir);
save_figs(hb,'fig', '', fdir); save_figs(hb,'png', '', fdir);
save_figs(hc,'fig', '', fdir); save_figs(hc,'png', '', fdir);
save_figs(hc2,'fig', '', fdir); save_figs(hc2,'png', '', fdir);
% save_figs(hx,'fig', '', fdir); save_figs(hx,'png', '', fdir);

%% Record runinfo of all tests 
fprintf('writting runinfos into log file...\n ');
writetable(struct2table(runinfos), sprintf('%s/runinfos.csv', fdir));

