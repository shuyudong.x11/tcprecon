clear; close all;

%% Information for generating the synthetic dataset

% Global test id 
tid = str2num(datestr(now, 'HHMMddmm')); 

GDATSIZES        = [300, 500, 200];
GDATRANK         = [3, 7, 5]; % Tucker rank  
GDAT_SCALE       = 5.0; 
GDATNOISE        = [1, 40]; % the first number being 0 means it is "noiseless". 

%% Generate or load data 
data = gen_tensor_low_tcrank('gdatSizes',       GDATSIZES,... 
                             'gdatRank',        GDATRANK,...
                             'gdatParam_scale', GDAT_SCALE,...
                             'gdatNoise',       GDATNOISE);

%% Set up the range of sampling rates
SR_MIN = 0.1; % 0.20;
SR_MAX = 5e-2; %0.20;
SR_N   = 1;
[SRs] = linspace(SR_MIN, SR_MAX, SR_N);

%% Generate problem parameters (Reg, CPD rank, Precon-delta)
IV_PARAMD = [1/2 1/2 1/2; 1/2 1/2 1/2];
IV_RANK   = floor([1.0; 2]*max(GDATRANK));
NHPs      = 2; 
[arr_hps, tab_hps] = gen_hps_2('do_randomsearch', [0, 0],...
                               'n_hps', NHPs, 'pb_paramd', IV_PARAMD, ...
                               'pb_rank', IV_RANK); 
arr_hps = arr_hps(end); 
tab_hps = tab_hps(end,:); 
fprintf('The parameter settings to test are:\n');
disp(tab_hps);

%% Generate the list of methods to test
% a customized tucker rank for KM16
tcr1 = repmat(ceil(0.8*arr_hps(1).rank), [1,3]); 
tcr2 = repmat(ceil(0.6*arr_hps(1).rank), [1,3]); 
tcr3 = repmat(ceil(0.5*arr_hps(1).rank), [1,3]); 

mids = {...
'Precon RGD linemin',
'Precon RCG linemin',
'Precon RGD RBB2',
'Euclidean CG linemin'...
% 'KM16 KM16 R,R,R',
% sprintf('KM16 KM16 %d,%d,%d',tcr1(1),tcr1(2),tcr1(3)),
% sprintf('KM16 KM16 %d,%d,%d',tcr2(1),tcr2(2),tcr2(3)),
% sprintf('KM16 KM16 %d,%d,%d',tcr3(1),tcr3(2),tcr3(3)),
% 'AltMin AltMin NA'...
% 'airCP airCP NA', %'INDAFAC INDAFAC NA',
% 'TNCP TNCP NA', 
% 'CP-WOPT CP-WOPT NA',
% 'HaLRTC HaLRTC NA'...
};
VALS_DELTA = [1e-7];  
[minfos, tab_m, mlabels] = gen_minfos(mids, 'precon_delta_rgrad', VALS_DELTA);  
fprintf('The methods to test are:\n');
disp(mlabels);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%% Set up optimization-related parameters
method_init = 'random'; %'HOSVD';  
TOL_RELCHG_RMSE = 1e-6;  % the new stopping criterion option  
MAXTIME = 300;
MAXIT = Inf;
TOL = 1e-6;
Nte = 1;
VERBO = 1; 

% Opt parameter for AltMin, the default setting of 1e-9 makes AltMin unbearably
% slow on this dataset. 
ALTMIN_TOL_CG = 1e-3; 

debug = false; 

res_rperi = cell(1,numel(minfos)); 
irun = 0; 
for i = 1 : numel(SRs)
    for tt = 1 : Nte
        tcinfo = LRTC.build_tcinfo_frSpT(data.tensor, SRs(i), tt);

        for j = 1 : numel(arr_hps)
            t0 = tic;
            Xinit = LRTC.initialize_cpd(tcinfo, arr_hps(j).rank, method_init); 
            tinit = toc(t0);
            for ii = 1 : numel(minfos)
                switch minfos(ii).manifold 
                    case 'Embedded'
                        error('The manifold structure embedded is not available'); 
                    case 'Euclidean'
                        % pb = LRTC('CPD_euc', data.tensor, tcinfo);
                        pb = LRTC('CPD_euc_mex', data.tensor, tcinfo);
                        pb.tid = tid; 
                        pb.stopfun = @(optpb, X, info, last)LRTC.stopfun_ml1m(optpb, X, info, last);
                    otherwise 
                        % case {'Precon','KM16','AltMin','INDAFAC','CP-WOPT','FaLRTC','SiLRTC','HaLRTC'} 
                        % pb = LRTC('CPD_precon', data.tensor, tcinfo);
                        pb = LRTC('CPD_precon_mex', data.tensor, tcinfo);                   
                        pb.tid = tid;
                        pb.stopfun = @(optpb, X, info, last)LRTC.stopfun_ml1m(optpb, X, info, last);
                end
                % Set up the CPD rank. The methods concerned are those
                % implmemented in Solver.m 
                pb.set_params_pb('rank', arr_hps(j).rank); 
                pb.set_params_pb('paramd', arr_hps(j).paramd);
                if debug
                    % Produce a zero-th iterate with meta-data only like [SR, rank, Ihp, Ite, (string) params]. 
                    X_ = nan;
                    stats_ = struct();
                    len = 1; 
                    srs  = num2cell(repmat(pb.tcinfo.SR, [1,len]));  
                    rks  = num2cell(repmat(pb.params_pb.rank, [1,len]));  
                    ihps = num2cell(repmat(j+pb.tid, [1,len]));  

                    %% Let the "ite" id include the time info: starting time of the initial time (day-hr-min-sec) of the experiment (scripts/test*.m). 
                    ites = num2cell(repmat(tt+pb.tid, [1,len]));  
                    hps  = {num2str(pb.params_pb.paramd, '%1.1e,')};  

                    [stats_.SR] = srs{:};
                    [stats_.rank] = rks{:};
                    [stats_.Ihp] = ihps{:};
                    [stats_.Ite] = ites{:};
                    [stats_.parampb] = deal(hps{:});
                    runinfo = [struct2table(pb.params_pb), struct2table(minfos(ii))];
                else
                    [X_, stats_, runinfo] = runmethods2_fromlist(pb, Xinit, minfos(ii), j, tt, ...
                                                   'altmin_tol_cg', ALTMIN_TOL_CG, ... 
                                                   'tol_relchg_rmse', TOL_RELCHG_RMSE, ... 
                                                   'maxtime', MAXTIME, 'maxiter', MAXIT,...
                                                   'tolgradnorm', TOL, 'verbosity', VERBO);
                end
                irun = irun + 1; 
                %% Collect results in a container 
                % res_rperi{i, ii, tt} = get_rperi_unified(stats_); 
                if ~debug
                    for t = 1 : numel(stats_)
                        stats_(t).time = stats_(t).time + tinit; 
                    end
                end
                % Append stats_ to the end of res_peri{ii}
                if isempty(res_rperi{ii})
                    res_rperi{ii} = stats_;
                else
                    res_rperi{ii} = [res_rperi{ii}, stats_];
                end
                testinfo = [struct2table(struct('rid', irun, 'SR', SRs(i),...
                                                'iHP', j, 'iTe', tt)), runinfo]; 
                runinfos(irun) = table2struct(testinfo);  
                fprintf('....Method: %s finished. The test info was: \n',...
                        minfos(ii).name); 
                disp(testinfo); 
            end
        end
   end
end

timeStr = sprintf('%s_%s', num2str(tid), datestr(now,'HHMMddmm'));  
%% Save results into a matfile
if debug
    dosave = false;
else
    dosave = true;
end
arr_m = save_res2_by_arr_m(res_rperi, minfos, data, timeStr, dosave); 

% Figs 1--2 
mshow_tr = {...
'Precon RGD (linemin)', 
'Precon RCG (linemin)',
'Precon RGD (RBB2)',
'Euclidean CG (linemin)', 
'Euclidean GD (BB)'...
};
mshow_t = {...
'Precon RGD (linemin)',
'Precon RCG (linemin)',
'Precon RGD (RBB2)',
'Euclidean CG (linemin)', 
'Euclidean GD (BB)'...
};

fprintf('Producing figures...\n ');
ha = figure();
ham = [];
legsa = {};
for j = 1 : numel(arr_m)
    if any(strcmp(arr_m(j).name, mshow_tr))
        legsa{end+1} = arr_m(j).name; 
        ham(end+1) = semilogy([arr_m(j).res.time], [arr_m(j).res.RMSE_tr]); hold on;
    end
end
xlabel('Time (second)'); 
ylabel('RMSE (train)'); 
legend(ham, legsa); 
hold off; 

hb = figure();
hbm = [];
legsb = {};
for j = 1 : numel(arr_m)
    if any(strcmp(arr_m(j).name, mshow_t))
        legsb{end+1} = arr_m(j).name; 
        hbm(end+1) = semilogy([arr_m(j).res.time], [arr_m(j).res.RMSE_t]); hold on;
    end
end
xlabel('Time (second)'); 
ylabel('RMSE (test)'); 
legend(hbm, legsb); 
hold off; 


% %% Save figures 
% fdir = sprintf('./figs/%s_%s',  data.name, timeStr); 
% if ~isdir(fdir)
%     mkdir(fdir);
% end
% 
% save_figs(ha,'fig', '', fdir); save_figs(ha,'png', '', fdir);
% save_figs(hb,'fig', '', fdir); save_figs(hb,'png', '', fdir);
% 
% %% Record runinfo of all tests 
% fprintf('writting runinfos into log file...\n ');
% writetable(struct2table(runinfos), sprintf('%s/runinfos.csv', fdir));

