% Test the various types of BB stepsize rules for our RGD algorithm. 
% Last update: May 2020.

clear; close all;

SNR_high = 60;
SNR_low = 30;
SNR = 40; 

SR_high = 0.3;
SR_low = 0.1;

test_rgeqstar_oursBBs(SR_high, SNR, 'faverable'); 
test_rgeqstar_oursBBs(SR_low, SNR, 'adverserial'); 


