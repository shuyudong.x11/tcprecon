clear; close all;

%% Load data: movie lens 1M 
dataname = 'ML1M'; 
data = load_tensor(dataname); 

%% Set up the range of sampling rates
SR_MIN = 0.8; 
SR_MAX = 0.8; 
SR_N   = 1;
[SRs] = linspace(SR_MIN, SR_MAX, SR_N);

%% List out algorithms to test 
% mids = {'Qprecon RGD RBB2', 'Qprecon RCG linemin', ...
%         'Euclidean GD RBB2', 'Euclidean CG linemin',...
%         'KM16 KM16 eqrks',...
%         'AltMin AltMin reg'};
mids = {...
    'Euclidean RGD BB2', ...
    'Euclidean RCG linemin', ...
    'Precon RGD RBB2', ...
    'Precon RCG linemin' ...
    % 'AltMin AltMin reg'...
    };
VALS_DELTA = [1e-7]; % 
[minfos, tab_m, mlabels] = gen_minfos(mids, 'precon_delta_rgrad', VALS_DELTA);  
fprintf('The methods to test are:\n');
disp(mlabels);
pause(3); 

%% List out rank values to test
RKs = [1:15, 17, 19, 21] ; 

%% Get regularization parameters from cross validation results 

[arr_hps] = get_crossval_params(RKs); 

% NOTE: 
% The parameters saved in the mat-file are as follows, 
% where 'paramd' is the 3D array containing the parameter lambda of the
% Frobenius norm-based regularizers and 'rank' is the PD rank parameter.  
% 
% +------------------------------------------------------------+ 
% 
%               paramd              rank
%     __________________________    ____
% 
%          2         2         2      5
%         10        10        10     10
%     15.874    15.874    15.874     15
% +------------------------------------------------------------+ 

% arr_hps = struct('paramd', [2,2,2], 'rank', 5); 
disp(struct2table(arr_hps));
pause(3); 

%% Set up optimization-related parameters
METHOD_INIT = 'random';  %'HOSVD'; 
TOL_RELCHG_RMSE = 1e-5;  % Stopping criterion option  
MAXTIMEs = [800; 1800];
MAXIT = Inf;
TOL = 1e-5; % tolgradnorm 
VERBO = 1; 

% % Opt parameter for AltMin, the default setting of 1e-9 makes AltMin unbearably
% % slow on this dataset. 
% ALTMIN_TOL_CG = 1e-3; 

%% Global test id 
tid = str2num(datestr(now, 'HHMMddmm')); 

res_rperi = cell(1,numel(minfos)); 
irun = 0; 
SEED = 1; 

% Generate training data from the dataset 
tcinfo = LRTC.build_tcinfo_frSpT(data.tensor, SRs(1), SEED);
% return; 
% Run tests
tt = 1 ; % repeat identical tests once. 

for j = 1 : numel(arr_hps)
    if arr_hps(j).rank > 10
        maxtime = MAXTIMEs(2);
    else
        maxtime = MAXTIMEs(1);
    end
    t0 = tic;
    Xinit = LRTC.initialize_cpd(tcinfo, arr_hps(j).rank, METHOD_INIT); 
    tinit = toc(t0);
    for ii = 1 : numel(minfos)
    % Define the problem object according to the manifold type of the
    % algorithm, the reg parameters and the rank parameter. 
        switch minfos(ii).manifold 
            case 'Euclidean'
                pb = LRTC('CPD_euc_mex', data.tensor, tcinfo);
                pb.tid = tid; 
                pb.stopfun = @(optpb, X, info, last) LRTC.stopfun_ml1m(optpb, X, info, last);
            case 'Precon' 
                pb = LRTC('CPD_precon_mex', data.tensor, tcinfo);                   
                pb.tid = tid;      
                pb.stopfun = @(optpb, X, info, last) LRTC.stopfun_ml1m(optpb, X, info, last);
            otherwise 
                % External algorithms 
                pb = LRTC('external', data.tensor, tcinfo);                   
                pb.tid = tid;      
                pb.stopfun = @(optpb, X, info, last) LRTC.stopfun_ml1m(optpb, X, info, last);
        end
        pb.set_params_pb('rank', arr_hps(j).rank); 
        pb.set_params_pb('paramd', arr_hps(j).paramd);

    % Solve the problem using one of the algorithms listed in
    % minfos. 
        [X_, stats_, runinfo] = runmethods2_fromlist(pb, Xinit, minfos(ii), j, tt, ...
                                       'altmin_tol_cg', ALTMIN_TOL_CG, ... 
                                       'tol_relchg_rmse', TOL_RELCHG_RMSE, ... 
                                       'maxtime', maxtime, 'maxiter', MAXIT,...
                                       'tolgradnorm', TOL, 'verbosity', VERBO);

    %% Collect results in a container 
    % res_rperi{i, ii, tt} = get_rperi_unified(stats_); 
        irun = irun + 1; 
        for t = 1 : numel(stats_)
            stats_(t).time = stats_(t).time + tinit; 
        end
    % Append stats_ to the end of res_peri{ii}
        if isempty(res_rperi{ii})
            res_rperi{ii} = stats_;
        else
            res_rperi{ii} = [res_rperi{ii}, stats_];
        end
        testinfo = [struct2table(struct('rid', irun, 'SR', SRs(1),...
                                        'iHP', j, 'iTe', tt)), runinfo]; 
        runinfos(irun) = table2struct(testinfo);  
        fprintf('....Method: %s finished. The test info was: \n',...
                minfos(ii).name); 
        disp(testinfo); 
    end
end

timeStr = sprintf('%s_%s', datestr(now,'HHMMddmm'), num2str(tid));  

%% Save results into a matfile
dosave = true;
arr_m = save_res2_by_arr_m(res_rperi, minfos, data, timeStr, dosave); 

% Figs 1--2 
mshow_tr = {...
'Precon RGD (linemin)', 
'Precon RCG (linemin)',
'Precon RGD (RBB2)',
'Euclidean CG (linemin)', 
'Euclidean GD (BB)'...
};
mshow_t = {...
'Precon RGD (linemin)',
'Precon RCG (linemin)',
'Precon RGD (RBB2)',
'Euclidean CG (linemin)', 
'Euclidean GD (BB)'...
};

fprintf('Producing figures...\n ');
ha = figure();
ham = [];
legsa = {};
for j = 1 : numel(arr_m)
    if any(strcmp(arr_m(j).name, mshow_tr))
        legsa{end+1} = arr_m(j).name; 
        ham(end+1) = semilogy([arr_m(j).res.time], [arr_m(j).res.RMSE_tr]); hold on;
    end
end
xlabel('Time (second)'); 
ylabel('RMSE (train)'); 
legend(ham, legsa); 
hold off; 

hb = figure();
hbm = [];
legsb = {};
for j = 1 : numel(arr_m)
    if any(strcmp(arr_m(j).name, mshow_t))
        legsb{end+1} = arr_m(j).name; 
        hbm(end+1) = semilogy([arr_m(j).res.time], [arr_m(j).res.RMSE_t]); hold on;
    end
end
xlabel('Time (second)'); 
ylabel('RMSE (test)'); 
legend(hbm, legsb); 
hold off; 

% %% Save figures 
% fdir = sprintf('./figs/%s_%s',  data.name, timeStr); 
% if ~isdir(fdir)
%     mkdir(fdir);
% end
% 
% save_figs(ha,'fig', '', fdir); save_figs(ha,'png', '', fdir);
% save_figs(hb,'fig', '', fdir); save_figs(hb,'png', '', fdir);
% 
% %% Record runinfo of all tests 
% fprintf('writting runinfos into log file...\n ');
% writetable(struct2table(runinfos), sprintf('%s/runinfos.csv', fdir));

