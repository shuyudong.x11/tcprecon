clear; close all;

%% Load data: movie lens 1M 
% dataname = 'ML100k'; 
dataname = 'ML1M'; 
data = load_tensor(dataname); 

%% Set up the range of sampling rates and the rank choices to test
SR = 0.85; 

% Get training and test data 
tcinfo = LRTC.build_tcinfo_frSpT(data.tensor, SR, 1); % the last input is the seed number 

%% Generate a set of rank choices for parameter selection.
IV_RANK   = [5];        % Rank values in cross validation tests for parameter selection.

% Generate problem parameters (Reg, CPD rank, Qprecon-delta)
% IV_PARAMD = [-1 -1 -1; log10(2) log10(2) log10(2)];
IV_PARAMD = [-4 -4 -4; 2/3 2/3 2/3];
NHPs      = 4;                  % Number of parameter configs to cross validate 
[arr_hps_1, tab_hps1] = gen_hps_onerk('do_randomsearch', [0, 0],...
                               'n_hps', NHPs, 'pb_paramd', IV_PARAMD, ...
                               'pb_rank', IV_RANK);

IV_RANK   = [10];        % Rank values in cross validation tests for parameter selection.

% Generate problem parameters (Reg, CPD rank, Qprecon-delta)
% IV_PARAMD = [1 1 1; log10(40) log10(40) log10(40)];
IV_PARAMD = [-4 -4 -4; 2/3 2/3 2/3];
NHPs      = 4;                  % Number of parameter configs to cross validate 
[arr_hps_2, tab_hps2] = gen_hps_onerk('do_randomsearch', [0, 0],...
                               'n_hps', NHPs, 'pb_paramd', IV_PARAMD, ...
                               'pb_rank', IV_RANK);

IV_RANK   = [15];        % Rank values in cross validation tests for parameter selection.

% Generate problem parameters (Reg, CPD rank, Qprecon-delta)
IV_PARAMD = [-4 -4 -4; 5/3 5/3 5/3];
NHPs      = 3;                  % Number of parameter configs to cross validate 
[arr_hps_3, tab_hps3] = gen_hps_onerk('do_randomsearch', [0, 0],...
                               'n_hps', NHPs, 'pb_paramd', IV_PARAMD, ...
                               'pb_rank', IV_RANK);

arr_hps = [arr_hps_1, arr_hps_2, arr_hps_3 ]; 
tab_hps = struct2table(arr_hps); 

fprintf('The parameter settings to test are:\n');
disp(tab_hps);
pause(3); 

%% Parameter selection via cross validation 
% Set up optimization-related parameters
METHOD_INIT = 'random';   
TOL_RELCHG_RMSE = 1e-5; 
MAXTIME = 800;
MAXIT = 10000;
TOL = 1e-5; 
Nte = 1; 
VERBO = 1;
ALTMIN_TOL_CG = 1e-3;  % Opt parameter for AltMin, the default setting of 1e-9 makes AltMin unbearably slow 


% Global test id 
TID = str2num(datestr(now, 'HHMMddmm')); 
pb = LRTC('CPD_precon_mex', data.tensor, tcinfo);                   
pb.tid = TID; 
pb.stopfun = @(optpb, X, info, last)LRTC.stopfun_ml1m(optpb, X, info, last);

% % Group of algorithms (CPD_AltMin): AltMin for CPD 
% name_group2 = 'CPD_AltMin'; 
% [hpsel_2, cvinfo2] = tc_paramsel(pb, name_group2, arr_hps, SR, TID,...
%                                            'tol_relchg_rmse', TOL_RELCHG_RMSE, ... 
%                                            'method_init', METHOD_INIT, ...
%                                            'altmin_tol_cg', ALTMIN_TOL_CG, ...
%                                            'maxtime', MAXTIME, 'maxiter', MAXIT,...
%                                            'tolgradnorm', TOL, 'verbosity', VERBO);
% 
% % Group of algorithms (CPD_GD): gradient-based algorithms for CPD 
% name_group1 = 'CPD_GD'; 
% [hpsel_1, cvinfo1] = tc_paramsel(pb, name_group1, arr_hps, SR, TID,...
%                                            'tol_relchg_rmse', TOL_RELCHG_RMSE, ... 
%                                            'method_init', METHOD_INIT, ...
%                                            'maxtime', MAXTIME, 'maxiter', MAXIT,...
%                                            'tolgradnorm', TOL, 'verbosity', VERBO);
% 

% Group of algorithms (Tucker_KM16): KM16 for Tucker decomposition  
name_group3 = 'KM16'; 
[hpsel_3, cvinfo3] = tc_paramsel(pb, name_group3, arr_hps, SR, TID,...
                                           'tol_relchg_rmse', TOL_RELCHG_RMSE, ... 
                                           'method_init', METHOD_INIT, ...
                                           'maxtime', MAXTIME, 'maxiter', MAXIT,...
                                           'tolgradnorm', TOL, 'verbosity', VERBO);


%% Record runinfo of all tests and save cross val results into mat-file

timeStr = sprintf('%s_%s', datestr(now,'HHMMddmm'), num2str(TID));  
fdir = sprintf('./figs/cv_%s_%s', dataname, timeStr); 
if ~isdir(fdir)
    mkdir(fdir);
end

fprintf('writting runinfos into log file...\n ');

if exist('cvinfo1', 'var')
    clearvars cvinfo hpsel; 
    cvinfo = cvinfo1; 
    hpsel = hpsel_1; 
    writetable(struct2table(cvinfo), sprintf('%s/cvinfo_%s.csv', fdir, name_group1));
    save(sprintf('%s/cvresults_%s.mat', fdir, name_group1), 'cvinfo', 'hpsel'); 
end
if exist('cvinfo2', 'var')
    clearvars cvinfo hpsel; 
    cvinfo = cvinfo2; 
    hpsel = hpsel_2; 
    writetable(struct2table(cvinfo), sprintf('%s/cvinfo_%s.csv', fdir, name_group2));
    save(sprintf('%s/cvresults_%s.mat', fdir, name_group2), 'cvinfo', 'hpsel'); 
end
if exist('cvinfo3', 'var')
    clearvars cvinfo hpsel; 
    cvinfo = cvinfo3; 
    hpsel = hpsel_3; 
    writetable(struct2table(cvinfo), sprintf('%s/cvinfo_%s.csv', fdir, name_group3));
    save(sprintf('%s/cvresults_%s.mat', fdir, name_group3), 'cvinfo', 'hpsel'); 
end

