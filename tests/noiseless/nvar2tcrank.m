% 


function tcr = nvar2tcrank(nvars, m, R)
% INPUT:
% nvars         Number of variables, ie., dimensions of the search space
% m             Dimensions of the tensor
% R             CP rank of the CPD model with 'nvars' variables
% OUTPUT:
% tcr           Dimensions of a cubic core tensor.

td_nvar = @(m,r) sum(m.*r)-sum(r.^2)+prod(r); 

for tempr = 1 : R 
    td_nvars(tempr) = td_nvar(m, tempr*ones(1,3)); 
end

diffs = nvars - td_nvars ;
diffs(diffs<0) = Inf; % discard all cases where td_nvars is larger in the selection by min


[~, ir] = min(diffs);

tcr = ir; 
end
