clear; close all;

%% Information for generating the synthetic dataset

% Global test id 
tid = str2num(datestr(now, 'HHMMddmm')); 

GDATSIZES        = [100, 100, 200];
GDATRANK         = [3, 5, 7]; % Tucker rank  
GDAT_SCALE       = 5.0; 
GDATNOISE        = [0, 30]; % the first number being 0 means it is "noiseless". 

%% Generate or load data 
% load datasets/syn_data_100_without.mat
data = gen_tensor_low_tcrank('gdatSizes',       GDATSIZES,... 
                             'gdatRank',        GDATRANK,...
                             'gdatParam_scale', GDAT_SCALE,...
                             'gdatNoise',       GDATNOISE);

%% Set up the range of sampling rates
SR_MIN = 0.1; % 0.20;
SR_MAX = 0.3; %0.20;
SR_N   = 1;
[SRs] = linspace(SR_MIN, SR_MAX, SR_N);

%% Generate problem parameters (Reg, CPD rank, Precon-delta)
RKS = [12; 14; 16]; 
PARAMDS = zeros(numel(RKS),3);
[arr_hps, tab_hps] = gen_hps_fixed(PARAMDS, RKS); 
fprintf('The parameter settings to test are:\n');
disp(tab_hps);

%% (#924.note)
% //passed//new script updated till here! 
pd_nvar = @(m,R) sum(m)*R; 
for i = 1  : numel(arr_hps)
    cpr = arr_hps(i).rank; 
    nb = pd_nvar(GDATSIZES, cpr);
    tcr = nvar2tcrank(nb, GDATSIZES, cpr); 
    km16_customized{i} = sprintf('KM16 KM16 %d,%d,%d', tcr,tcr,tcr);
end
TCRX_TESTED = false;

% Generate the list of methods to test

mids = {...
'Precon RGD linemin',
'Precon RCG linemin',
'Precon RGD RBB2',
'Euclidean GD linemin',
'Euclidean CG linemin',
'Euclidean GD BB'...
% 'KM16 KM16 R,R,R',
% 'AltMin AltMin NA',
% 'INDAFAC INDAFAC NA',
% 'CP-WOPT CP-WOPT NA',
% 'FaLRTC FaLRTC NA',
% 'SiLRTC SiLRTC NA',
% 'HaLRTC HaLRTC NA'...
};

VALS_DELTA = [1e-7]; %[0, 1e-3]; %[0, 1e-6, 1e-4, 1e-2]; 
[minfosa, tab_m, mlabels] = gen_minfos(mids, 'precon_delta_rgrad', VALS_DELTA);  
[minfosb, tab_mb, mlabelsb] = gen_minfos(km16_customized, 'precon_delta_rgrad', VALS_DELTA);  

fprintf('The methods to test are:\n');
minfos = [minfosa, minfosb];
tab_m = [tab_m; tab_mb];
mlabels = table2cell(tab_m(:,'name')); 
disp(mlabels);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% return; 

%% Set up optimization-related parameters
method_init = 'random'; %'HOSVD';  %size=[100,100,100]
MAXTIME = 100;
MAXIT = 10000;
TOL = 1e-7;
Nte = 1;
VERBO = 1; 

% % Opt parameter for AltMin, the default setting of 1e-9 makes AltMin unbearably
% % slow on this dataset. 
% ALTMIN_TOL_CG = 1e-5; 

debug = false; 

res_rperi = cell(1,numel(minfos)); 
irun = 0; 
for i = 1 : numel(SRs)
    for tt = 1 : Nte
        tcinfo = LRTC.build_tcinfo_frSpT(data.tensor, SRs(i), tt);

        for j = 1 : numel(arr_hps)
            t0 = tic;
            Xinit = LRTC.initialize_cpd(tcinfo, arr_hps(j).rank, method_init); 
            tinit = toc(t0);
            if j > 1 
                TCRX_TESTED = true;
            end
            for ii = 1 : numel(minfos)
                switch minfos(ii).manifold 
                    case 'Embedded'
                        error('The manifold structure embedded is not available'); 
                    case 'Euclidean'
                        % pb = LRTC('CPD_euc', data.tensor, tcinfo);
                        pb = LRTC('CPD_euc_mex', data.tensor, tcinfo);
                        pb.tid = tid; 
                    case 'KM16'
                        if ~strcmp(minfos(ii).descriptor,'R,R,R') && TCRX_TESTED 
                            fprintf('KM16 customized rank tested, skip..\n');
                            break;
                        end
                        pb = LRTC('CPD_precon_mex', data.tensor, tcinfo);                   
                        pb.tid = tid;
                    otherwise 
                        % case {'Precon','KM16','AltMin','INDAFAC','CP-WOPT','FaLRTC','SiLRTC','HaLRTC'} 
                        % pb = LRTC('CPD_precon', data.tensor, tcinfo);
                        pb = LRTC('CPD_precon_mex', data.tensor, tcinfo);                   
                        pb.tid = tid;
                end
                % Set up the CPD rank. The methods concerned are those
                % implmemented in Solver.m 
                pb.set_params_pb('rank', arr_hps(j).rank); 
                pb.set_params_pb('paramd', arr_hps(j).paramd);
                if debug
                    % Produce a zero-th iterate with meta-data only like [SR, rank, Ihp, Ite, (string) params]. 
                    X_ = nan;
                    stats_ = struct();
                    len = 1; 
                    srs  = num2cell(repmat(pb.tcinfo.SR, [1,len]));  
                    rks  = num2cell(repmat(pb.params_pb.rank, [1,len]));  
                    ihps = num2cell(repmat(j+pb.tid, [1,len]));  

                    %% Let the "ite" id include the time info: starting time of the initial time (day-hr-min-sec) of the experiment (scripts/test*.m). 
                    ites = num2cell(repmat(tt+pb.tid, [1,len]));  
                    hps  = {num2str(pb.params_pb.paramd, '%1.1e,')};  

                    [stats_.SR] = srs{:};
                    [stats_.rank] = rks{:};
                    [stats_.Ihp] = ihps{:};
                    [stats_.Ite] = ites{:};
                    [stats_.parampb] = deal(hps{:});
                    runinfo = [struct2table(pb.params_pb), struct2table(minfos(ii))];
                else
                    [X_, stats_, runinfo] = runmethods2_fromlist(pb, Xinit, minfos(ii), j, tt, ...
                                                   'altmin_tol_cg', ALTMIN_TOL_CG, ... 
                                                   'maxtime', MAXTIME, 'maxiter', MAXIT,...
                                                   'tolgradnorm', TOL, 'verbosity', VERBO);
                end
                irun = irun + 1; 
                %% Collect results in a container 
                % res_rperi{i, ii, tt} = get_rperi_unified(stats_); 
                if ~debug
                    for t = 1 : numel(stats_)
                        stats_(t).time = stats_(t).time + tinit; 
                    end
                end
                % Append stats_ to the end of res_peri{ii}
                if isempty(res_rperi{ii})
                    res_rperi{ii} = stats_;
                else
                    res_rperi{ii} = [res_rperi{ii}, stats_];
                end
                testinfo = [struct2table(struct('rid', irun, 'SR', SRs(i),...
                                                'iHP', j, 'iTe', tt)), runinfo]; 
                runinfos(irun) = table2struct(testinfo);  
                fprintf('....Method: %s finished. The test info was: \n',...
                        minfos(ii).name); 
                disp(testinfo); 
            end
        end
   end
end

timeStr = sprintf('%s_%s', num2str(tid), datestr(now,'HHMMddmm'));  
%% Save results into a matfile
if debug
    dosave = false;
else
    dosave = true;
end
arr_m = save_res2_by_arr_m(res_rperi, minfos, data, timeStr, dosave); 
% 
% fdir = sprintf('./figs/%s_%s',  data.name, timeStr); 
% if ~isdir(fdir)
%     mkdir(fdir);
% end
% %% Record runinfo of all tests 
% fprintf('writting runinfos into log file...\n ');
% writetable(struct2table(runinfos), sprintf('%s/runinfos.csv', fdir));


